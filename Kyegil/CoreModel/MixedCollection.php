<?php

namespace Kyegil\CoreModel;


use Kyegil\CoreModel\Interfaces\AppInterface;
use Kyegil\CoreModel\Interfaces\CoreModelCollectionInterface;
use Kyegil\CoreModel\Interfaces\CoreModelInterface;
use Kyegil\CoreModel\Interfaces\CoreModelRepositoryInterface;
use mysqli;
use stdClass;

/**
 * Class MixedCollection
 * @package Kyegil\CoreModel
 */
class MixedCollection extends CoreModelCollection
{
    /** @var stdClass The different collections */
    protected stdClass $collections;
    /** @var null|class-string<CoreModelInterface>[] */
    protected ?array $models = [];

    /**
     * CoreModelCollection constructor.
     *
     * @param array $collections
     * @param array $di Dependencies
     * @throws CoreModelDiException
     * @noinspection PhpMissingParentConstructorInspection
     */
    public function __construct(
        array $collections,
        array $di
    )
    {
        $diRequired = [
            AppInterface::class,
            mysqli::class,
            CoreModelRepositoryInterface::class,
        ];
        foreach($diRequired as $class) {
            if(!isset($di[$class]) || !is_a($di[$class], $class)) {
                throw new CoreModelDiException("Required instance of " . $class . " has not been injected to " . static::class . " constructor");
            }
        }

        $this->app = $di[AppInterface::class];
        $this->mysqli = $di[mysqli::class];
        $this->collections = new stdClass();

        foreach($collections as $collection) {
            if($collection instanceof CoreModelCollectionInterface) {
                $model = $collection->getModel();
            }
            else {
                $model = $collection;
                $collection = $this->app->getModelCollection($model);
            }
            $this->models[] = $model;
            $this->collections->$model = $collection;
        }
    }

    /**
     * Set filters to all models
     *
     * @param array $filters
     * @param $doNotMold
     * @return $this
     */
    public function setFilters(array $filters, $doNotMold = false): MixedCollection
    {
        foreach($this->collections as $collection) {
            $collection->setFilters($filters, $doNotMold);
        }
        return $this;
    }

    /**
     * Add a filter to all models
     *
     * @param $filter
     * @param bool $doNotMold
     * @return $this
     */
    public function addFilter($filter, bool $doNotMold = false): MixedCollection
    {
        $this->items = null;
        foreach($this->collections as $collection) {
            $collection->addFilter($filter, $doNotMold);
        }
        return $this;
    }

    /**
     * @return $this
     */
    public function resetFilters(): MixedCollection
    {
        foreach($this->collections as $collection) {
            $collection->resetFilters();
        }
        return $this;
    }

    /**
     * Resets all collections
     *
     * @return $this
     */
    public function reset(): MixedCollection
    {
        foreach($this->collections as $collection) {
            $collection->reset();
        }
        $this->items = null;
        return $this;
    }

    /**
     * @param bool $force
     * @return $this
     * @throws CoreModelException
     */
    public function load(bool $force = false): MixedCollection
    {
        if ($force || !is_array($this->items)) {
            $this->items = [];
            $this->totalRows = 0;
            /** @var CoreModelCollection $collection */
            foreach($this->collections as $collection) {
                $collection->load($force);
                $this->items = array_merge($this->items, $collection->getItemsArray());
                $this->totalRows += $collection->getTotalRows();
            }
            $this->chunkRows = count($this->items);
        }
        return $this;
    }

    /**
     * Fill the collection from an array of objects
     *
     * @param CoreModel[] $array
     * @return $this
     */
    public function loadFromArray(array $array): MixedCollection
    {
        $grouping = new stdClass();
        foreach ($array as $object) {
            if($object instanceof CoreModelInterface) {
                settype($grouping->{get_class($object)}, 'array');
                $grouping->{get_class($object)}[] = $object;
                $this->items[] = $object;
            }
        }

        // Clear existing values
        foreach($this->collections as $existingCollection) {
            $existingCollection->loadFromArray([]);
        }
        foreach($grouping as $model => $subArray) {
            if(isset($this->collections->$model)) {
                $this->collections->$model->loadFromArray($subArray);
            }
            else {
                $this->models[] = $model;
                $this->collections->$model = $this->app->getModelCollection($model)->loadFromArray($subArray);
            }
        }
        $this->totalRows = count($this->items);
        $this->chunkRows = count($this->items);
        $this->isAmended = true;
        return $this;
    }

    /**
     * @return string
     */
    public function getModel(): string
    {
        return '';
    }

    /**
     * @param array $joins
     * @return $this
     */
    public function setJoins(array $joins): MixedCollection
    {
        return $this;
    }

    /**
     * @param string|array|stdClass $table Either passed as table name, or as alias => table name
     * @param string $condition
     * @return $this
     */
    public function addLeftJoin($table, string $condition): MixedCollection
    {
        return $this;
    }

    /**
     * @param string|array|stdClass $table Either passed as table name, or as alias => table name
     * @param string $condition
     * @return $this
     */
    public function addInnerJoin($table, string $condition): MixedCollection
    {
        return $this;
    }

    /**
     * @param string $field
     * @param bool $descending
     * @param string|null $table
     * @param bool $nullAsMax
     * @return $this
     */
    public function addSortOrder(string $field, bool $descending = false, ?string $table = null, bool $nullAsMax = false): MixedCollection
    {
        return $this;
    }

    /**
     * @param array $groupFields
     * @return $this
     */
    public function setGroupFields(array $groupFields): MixedCollection
    {
        return $this;
    }

    /**
     * @param string $groupField
     * @return $this
     */
    public function addGroupField(string $groupField): MixedCollection
    {
        return $this;
    }

    /**
     * Save a set of scalar values to all items in the collection;
     *
     * @param string $property The property to udate
     * @param mixed $value The new value which will be applied to all items in the collection
     * @return $this
     */
    public function setData(string $property, $value): MixedCollection
    {
        return $this;
    }

    /**
     * Returns true if the collection was amended since it was loaded,
     * or if it was loaded manually by the loadFromArray()
     *
     * @return bool
     */
    public function isAmended(): bool
    {
        return $this->isAmended;
    }

    /**
     * Return nothing
     *
     * @return array
     */
    public function getItemIds(): array
    {
        return [];
    }
}