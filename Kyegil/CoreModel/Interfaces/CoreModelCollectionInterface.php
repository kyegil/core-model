<?php

namespace Kyegil\CoreModel\Interfaces;


use Closure;
use Iterator;
use stdClass;


/**
 *
 */
interface CoreModelCollectionInterface extends Iterator
{
    /**
     * Return the current element in a foreach loop
     *
     *  Required by the \Iterator interface
     *
     * @return CoreModelInterface|null
     * @see Iterator::current
     * @link https://www.php.net/manual/en/iterator.current.php
     */
    public function current(): ?CoreModelInterface;

    /**
     * Return the key of the current element
     *
     * Required by the \Iterator interface
     *
     * @return int
     * @link https://www.php.net/manual/en/iterator.key.php
     * @see Iterator::key
     */
    public function key(): int;

    /**
     * Move forward to next element
     *
     * Required by the \Iterator interface
     *
     * @link https://www.php.net/manual/en/iterator.next.php
     * @see Iterator::next
     */
    public function next();

    /**
     * Rewind the Iterator to the first element
     *
     * Required by the \Iterator interface
     *
     * @link https://www.php.net/manual/en/iterator.rewind.php
     * @see Iterator::rewind
     */
    public function rewind();

    /**
     * Checks if current position is valid
     * Required by the \Iterator interface
     *
     * @link https://www.php.net/manual/en/iterator.valid.php
     * @return bool
     */
    public function valid(): bool;

    /**
     * @param string $event
     * @param callable $callable
     * @return $this
     */
    public function on(string $event, callable $callable): self;

    /**
     * @param string $event
     * @return callable[]
     */
    public function getListeners(string $event): array;

    /**
     * @param string $event
     * @return $this
     */
    public function clearListeners(string $event): self;

    /**
     * Set the main model fields
     *
     * If not all the main model fields are required,
     * the ones needed can be set here for a more efficient query.
     *
     * Passing null will reset to include all fields of the model.
     * The primary key will always be included
     *
     * Passing null will reset to all
     *
     * @param string[]|null $fields
     * @return $this
     */
    public function setMainModelFields(?array $fields = null): self;
    
    /**
     * Add a field to the db query, in addition to the models main fields.
     *
     * Any existing fields with the same field name/alias will be overwritten.
     *
     * Adding a field to the query which has already loaded, will not force a reload.
     * In order to force the query to load with the new fields, the existing data must first be unloaded
     * using the `CoreModelCollectionInterface::unload()` method
     *
     * @param string $table
     * @param string $field
     * @param string|null $aggregate
     * @param string|null $property
     * @param string|null $namespace
     * @return $this
     */
    public function addField(
        string $table,
        string $field,
        ?string $aggregate = null,
        ?string $property = null,
        ?string $namespace = null
    ): self;

    /**
     * Add a formula expression to the db query
     *
     * This method will not force a reload of existing data.
     * In order to force the query to load with the new fields, the existing data must first be unloaded
     * using the `CoreModelCollectionInterface::unload()` method
     *
     * @param string $namespace
     * @param string $property
     * @param string $expression
     * @param string|null $aggregate
     * @return $this
     */
    public function addExpression(
        string $namespace,
        string $property,
        string $expression,
        ?string $aggregate = null
    ): self;

    /**
     * Add all the fields of an external model to the collection.
     *
     * The table joins must be added separately
     *
     * @param string $model
     * @param string|null $namespace
     * @param string|null $source
     * @return $this
     */
    public function addModel(string $model, ?string $namespace = null, ?string $source = null): self;

    /**
     * List all the fields that have been reguested in addition to the ones provided by the model
     *
     * @return stdClass[]
     */
    public function getAdditionalFields(): array;

    /**
     * Format all additional fields into valid SQL syntax using namespaces
     *
     * Examples:
     *
     * * field: 'field'
     * * table: 'table'
     * * property: 'property'
     * * namespace: 'namespace'
     * ```
     * `table`.`field` AS `namespace.property`
     * ```
     *
     * * field: 'field'
     * * table: 'table'
     * * property: 'property'
     * * namespace: 'custom.namespace'
     * * aggregate: 'SUM'
     * ```
     * SUM(`table`.`field`) AS `custom.namespace.property`
     * ```
     *
     * * expression: '`price` * `quantity_in_stock`'
     * * property: 'value'
     * * namespace: 'products'
     * ```
     * `price` * `quantity_in_stock` AS `products.value`
     * ```
     *
     * * expression: '`price` * `quantity_in_stock`'
     * * property: 'average_value'
     * * namespace: 'inventories'
     * * aggregate: 'AVG'
     * ```
     * AVG(`price` * `quantity_in_stock`) AS `inventories.average_value`
     * ```
     *
     * @return string[]
     */
    public function stringifyAdditionalFieldsForQuery(): array;

    /**
     * Clear all the additional fields
     *
     * Removes all fields except from the main model from the query.
     *
     * @return $this
     */
    public function clearAdditionalFields(): self;

    /**
     * Replace all the JOIN statements in one go
     *
     * The table alias should be used as a unique reference (key) for each join
     * *
     * @param array<string, string> $joins
     * @return $this
     */
    public function setJoins(array $joins): self;

    /**
     * Add an INNER JOIN statement
     *
     * @param string|array|stdClass $table Either passed as table name, or as alias => table name
     * @param string $condition
     * @return $this
     */
    public function addInnerJoin($table, string $condition): self;

    /**
     * Add a LEFT JOIN statement
     *
     * @param string|array<string, string> $table Either passed as table name, or as alias => table name
     * @param string $condition
     * @return $this
     */
    public function addLeftJoin($table, string $condition): self;

    /**
     * Add a JOIN statement containing a subquery
     *
     * @param string $subQuery
     * @param string $alias
     * @param string $joinCondition
     * @param string $joinType LEFT|INNER
     * @param array $bindValues Potential values to add to the prepared statement
     * @return $this
     */
    public function addSubQueryJoin(
        string $subQuery,
        string $alias,
        string $joinCondition,
        string $joinType = 'LEFT',
        array $bindValues = []
    ): self;

    /**
     * Get all the current JOIN statements
     *
     * The table alias is used as the array key for each join
     *
     * @return array<string, string>
     */
    public function getJoins(): array;

    /**
     * Remove an existing JOIN statement by its alias
     *
     * @param string $alias
     * @return $this
     */
    public function removeJoin(string $alias): self;

    /**
     * Get the bind values used by joins
     *
     * When filtering a query using values from a joined table,
     * the requested value will be stored in this
     *
     * @param string|null $alias
     * @return array
     * @see self::$joins
     */
    public function getBindValuesForJoins(?string $alias = null): array;

    /**
     * Is this collection reducable?
     *
     * A collection cannot be reduced (filtered or limited)
     * if it is a linked subcollection for another collection
     *
     * @return bool
     */
    public function isReducable(): bool;
    /**
     * Set WHERE filters to the collection.
     * The filters will NOT replace any existing locked filters
     *
     * @param array $filters
     * @param boolean $doNotMold Set true to apply raw filters
     * @return $this
     */
    public function setFilters(array $filters, bool $doNotMold = false): self;

    /**
     * Add ONE new WHERE filter to the existing filters
     * The existing collection items are unloaded when a filter is applied.
     *
     * The filter can either be given as a prepared predicate string 'id IS NULL',
     * or in the array format ['expression' => value]
     * in which case the value will be bound to a placeholder
     *
     * @param string|string[]|array<string,mixed> $filter
     * @param boolean $doNotMold Set true to apply raw filters
     * @return $this
     */
    public function addFilter($filter, bool $doNotMold = false): self;

    /**
     * Limit the collection to certain ids
     *
     * @param array $itemIds
     * @return $this
     */
    public function filterByItemIds(array $itemIds): self;

    /**
     * Return all the WHERE filters
     *
     * @return array
     */
    public function getFilters(): array;

    /**
     * Reset all the filters (remove all filters except from locked filters)
     *
     * The collection items unload when filters change.
     *
     * @return $this
     */
    public function resetFilters(): self;

    /**
     * Lock the current filters to the collection,
     * so they cannot be removed
     *
     * @return $this
     */
    public function lockFilters(): self;

    /**
     * Mold the filters using backticks if desirable
     *
     * This function tries to add backticks to a filter if none exist already.
     * Using backticks will improve clarity in field references
     *
     * Example:
     * * 'my_field' could be molded as `main_table`.`my_field`
     *
     * @param $filter
     * @return array|string
     */
    public function moldFilters($filter);

    /**
     * Prepare Filters Query
     *
     * Preparing a combination of predicates for use in a 'where' or 'having'-condition
     * Each element can be a ready-made string predicate,
     * or an array consisting of expression => value.
     * The expression can be a prepared string using ? as placeholders,
     * or end with a comparison operator:
     * one of '=', '>', '<', '<>', '<=', '>=', 'IS', 'NOT', 'LIKE' or 'IN'
     * If no comparison operator is given, '=' or 'IS' will be added.
     *
     * Expressions can be combined logically
     * using 'and', 'or', 'xor', 'not', '&&', '||' or '!'
     * as the element key
     *
     * @param string $expression
     * @param string|string[]|array[] $value
     * @param array $bindValues
     * @return string
     */
    public function prepareFiltersQuery(string $expression, $value, array &$bindValues): string;

    /**
     * Set HAVING filters to the collection.
     * The filters will replace any existing HAVING filters
     *
     * @param array $havingFilters
     * @param boolean $doNotMold Set true to apply raw filters
     * @return $this
     */
    public function setHaving(array $havingFilters, bool $doNotMold = false): self;

    /**
     * Add ONE new HAVING filter to the existing filters
     * The collection items unload when a filter is applied.
     *
     * The filter can either be given in the array format ['id' => null] or as a string 'id IS NULL'
     *
     * @param string|array $havingFilter
     * @param boolean $doNotMold Set true to apply raw filters
     * @return $this
     */
    public function addHaving($havingFilter, bool $doNotMold = false): self;

    /**
     * Return the HAVING filters
     *
     * @return array
     */
    public function getHaving(): array;

    /**
     * Set all the GROUP BY fields
     *
     * @param array $groupFields
     * @return $this
     */
    public function setGroupFields(array $groupFields): self;

    /**
     * Add a GROUP BY field
     *
     * @param string $groupField
     * @return $this
     */
    public function addGroupField(string $groupField): self;

    /**
     * Get the current GROUP BY fields
     *
     * @return string[]
     */
    public function getGroupFields(): array;

    /**
     * Add a SORT BY field
     *
     * The sort order field must be one of the requested fields.
     *
     * The sort order field should be provided as a simple field name string,
     * without quotes or table notation.
     *
     * If the field does not exist in the main table,
     * then the table name should be provided in the third argument.
     *
     * @param string $field
     * @param bool $descending
     * @param string|null $table
     * @param bool $nullAsMax
     * @return $this
     */
    public function addSortOrder(string $field, bool $descending = false, ?string $table = null, bool $nullAsMax = false): self;

    /**
     * Returns a list of SORT BY fields with their ASC/DESC direction
     *
     * @return string[]
     */
    public function getSortOrder(): array;

    /**
     * Reset the SORT BY fields
     *
     * @return $this
     */
    public function resetSortOrder(): self;

    /**
     * Set the start row when retrieving limited chunks (paging)
     *
     * @param int $start
     * @return $this
     */
    public function setStart(int $start): self;

    /**
     * Get the current start position
     *
     * @return int
     */
    public function getStart(): int;

    /**
     * Set the LIMIT
     *
     * @param int|null $limit
     * @return $this
     */
    public function setLimit(?int $limit = null): self;

    /**
     * Return the current LIMIT
     *
     * @return int|null
     */
    public function getLimit(): ?int;

    /**
     * Set chunk (page) number for limited collections
     *
     * @param int $chunk
     * @return $this
     */
    public function setChunk(int $chunk): self;

    /**
     * Load the collection
     *
     * @param bool $force
     * @return $this
     * @see loadFromArray()
     */
    public function load(bool $force = false): self;

    /**
     * Populate the collection from an array of existing objects
     *
     * @param CoreModelInterface[] $array
     * @return $this
     * @see load()
     */
    public function loadFromArray(array $array): self;

    /**
     * Preload all the Eav attributes for the entire collection
     *
     * @return $this
     * @todo Implement collection pre-loading of eav attributes.
     */
    public function loadAllEavAttributes(): self;

    /**
     * Unload the collection items, forcing the collection to reload if required
     *
     * @return $this
     * @see load()
     */
    public function unload(): self;

    /**
     * Check if the collection has loaded
     *
     * @return bool
     * @see load()
     */
    public function hasLoaded(): bool;

    /**
     * Iterate through the collection with a callable
     * Each item will be passed to the callable as the only argument
     *
     * @param callable $action
     * @return $this
     */
    public function forEach(callable $action): self;

    /**
     * Convert the collection to an array of models
     *
     * @return CoreModelInterface[]
     */
    public function getItemsArray(): array;

    /**
     * Return all the id values in the collection
     *
     * @return string[]
     */
    public function getItemIds(): array;

    /**
     * Converts a stdClass raw db data object to a CoreModel instance
     *
     * @param object|CoreModelInterface $item
     * @return CoreModelInterface
     */
    public function itemToModel($item): CoreModelInterface;

    /**
     * Convert all data items to CoreModel instances
     *
     * @return $this
     */
    public function formatAllItems(): self;

    /**
     * Get the model at the specified index
     *
     * @param int $index
     * @return CoreModelInterface|null
     */
    public function getAt(int $index): ?CoreModelInterface;

    /**
     * Get the current item
     *
     * @return CoreModelInterface|null
     */
    public function getCurrentItem(): ?CoreModelInterface;

    /**
     * Return the first item in the collection
     *
     * @return CoreModelInterface|null
     */
    public function getFirstItem(): ?CoreModelInterface;

    /**
     * Return a random item from the collection
     *
     * @return CoreModelInterface|null
     */
    public function getRandomItem(): ?CoreModelInterface;

    /**
     * Return the last item in the collection
     *
     * @return CoreModelInterface|null
     */
    public function getLastItem(): ?CoreModelInterface;

    /**
     * Get the size of the current chunk of data if using LIMIT
     *
     * @return int
     */
    public function getChunkRows(): int;

    /**
     * Get the total row count ignoring any LIMIT
     *
     * @return int
     */
    public function getTotalRows(): int;

    /**
     * Mix/merge this collection with another
     *
     * @param CoreModelCollectionInterface $collection
     * @return CoreModelCollectionInterface
     */
    public function mixWith(CoreModelCollectionInterface $collection): CoreModelCollectionInterface;

    /**
     * Remove a single item from the collection
     *
     * WARNING: Amending a collection this way will make it unreliable for reloading
     *
     * @param CoreModelInterface $itemToRemove
     * @return $this
     * @see isAmended()
     */
    public function removeItem(CoreModelInterface $itemToRemove): self;

    /**
     * Reverse the order of all the items in the collection
     *
     * WARNING: Amending a collection this way will make it unreliable for reloading
     *
     * @return $this
     * @see isAmended()
     */
    public function reverse(): self;

    /**
     * Slice the db result set after it has loaded
     *
     * WARNING: Amending a collection this way will make it unreliable for reloading
     *
     * @param int $offset
     * @param int|null $length
     * @return $this
     * @see isAmended()
     */
    public function slice(int $offset, ?int $length = null): self;

    /**
     * Sort all the items by a custom function after loading
     *
     * WARNING: Amending a collection this way will make it unreliable for reloading
     * *
     * @param Closure $sortfunction
     * @return $this
     * @see isAmended()
     */
    public function sortLoadedItems(Closure $sortfunction): self;

    /**
     * Returns true if the collection was amended since it was loaded
     *
     * If the collection has been amended, then all filters etc may be invalid
     * and the collection should not be relied on for reloading
     *
     * @return bool
     * @see load()
     */
    public function isAmended(): bool;

    /**
     * Save a value to all items in the collection;
     *
     * @param string $property The property to udate
     * @param mixed $value The new value which will be applied to all items in the collection
     * @return $this
     */
    public function setData(string $property, $value): self;

    /**
     * Save a set of values to all items in the collection
     *
     * @param stdClass $values The properties to update for all items in the collection, and their new values
     * @return $this
     */
    public function setMultipleData(stdClass $values): self;

    /**
     * Mass delete from the database
     *
     * Warning: This 'Delete' method will force delete the entries from the database,
     * without additional sanity checks etc.
     *
     * To delete each object the safe way, use deleteEach() instead
     *
     * @return $this
     * @see deleteEach()
     */
    public function massDelete(): self;

    /**
     * Delete each object in a collection
     *
     * @return CoreModelCollectionInterface
     */
    public function deleteEach(): self;

    /**
     * Get the name of the model class used by the collection
     *
     * @return class-string<CoreModelInterface>
     */
    public function getModel(): string;

    /**
     * Get the current or last used DB query
     *
     * @return string|null
     */
    public function getQuery(): ?string;

    /**
     * Include another collection based on its one-2-many relation
     *
     * @param string $reference
     * @param CoreModelCollectionInterface|null $collection
     * @param string|null $model
     * @param string|null $foreignKey
     * @param array $filters
     * @param callable|null $callback
     * @return CoreModelCollectionInterface|null
     */
    public function linkCollection(string $reference, ?CoreModelCollectionInterface $collection = null, ?string $model = null, ?string $foreignKey = null, array $filters = [], ?callable $callback = null): ?CoreModelCollectionInterface;

    /**
     * @param CoreModelCollectionInterface $collection
     * @param string $reference
     * @param string|null $model
     * @param string|null $foreignKey
     * @param array $filters
     * @param callable|null $callback
     * @return CoreModelCollectionInterface|null
     */
    public function willFollow(
        CoreModelCollectionInterface $collection,
        string  $reference,
        ?string $model = null,
        ?string $foreignKey = null,
        array   $filters = [],
        ?callable $callback = null

    ): ?CoreModelCollectionInterface;

    /**
     * Looks up the collection for which this is the subcollection source
     *
     * @return CoreModelCollectionInterface|null
     */
    public function getCollectionThisIsSubCollectionSourceFor(): ?CoreModelCollectionInterface;

    /**
     * Check if this collection is linked to (following) another collection
     *
     * @return object{reference: string, configuraton: array, collection: CoreModelCollectionInterface}|null
     */
    public function follows(): ?object;

    /**
     * Reset filters and prepare the collection for reloading
     *
     *  The collection items unload when a filter changes.
     *
     * @return $this
     */
    public function reset(): self;
}