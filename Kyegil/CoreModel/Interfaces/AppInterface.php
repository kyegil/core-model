<?php

namespace Kyegil\CoreModel\Interfaces;


use stdClass;

/**
 * Interface AppInterface
 * @package Kyegil\CoreModel\Interfaces
 */
interface AppInterface
{
    /**
     * @param string $class
     * @param $id
     * @return CoreModelInterface
     */
    public function getModel(string $class, $id): CoreModelInterface;

    /**
     * @param string $class
     * @param stdClass $params
     * @return CoreModelInterface
     */
    public function newModel(string $class, stdClass $params = null): CoreModelInterface;

    /**
     * @param string|array $class
     * @param bool $reuse
     * @param string $id
     * @return CoreModelCollectionInterface
     */
    public function getModelCollection($class, bool $reuse = false, string $id = ''): CoreModelCollectionInterface;

    /**
     * @return mixed
     */
    public function dispatchEvent($event, $args = null);

    /**
     * @param string $camelCase
     * @return string
     */
    public function makeUnderscore(string $camelCase): string;

    /**
     * @param string $underscore
     * @return string
     */
    public function makeCamelCase(string $underscore): string;

    /**
     * Before Method
     *
     * @param $subject
     * @param string $method
     * @param array $args
     * @return array
     */
    public function before($subject, string $method, array $args): array ;

    /**
     * After method
     *
     * @param $subject
     * @param string $method
     * @param $result
     * @return mixed
     */
    public function after($subject, string $method, $result);
}