<?php

namespace Kyegil\CoreModel\Interfaces;


use Kyegil\CoreModel\CoreModelException;
use stdClass;

interface CoreModelInterface
{
    /**
     * Get the name of the db table storing this model's instances
     *
     * @return string
     */
    public static function getDbTable(): string;

    /**
     * Get Db fields
     *
     * Returns an associated array with the fields of the main object table as key,
     * and an associated array as configuration for each of the fields
     *
     * Possible config options:
     * * type: The data type. Defaults to 'string'. 'string'|'int'|'bool'|'date'|'time'|'datetime'|'interval'|'json',
     *      the name of a \Kyegil\CoreModel\Interfaces\CoreModelInterface class,
     *      or a callable casting the string value in the correct type
     *  * allowNull: boolean. Defaults to true. False will disallow null values
     *  * allowMultiple: boolean. Defaults to false. True will allow multiple instances of the chosen type.
     *      Use with care, as this will complicate table joining.
     *  * target: string. If set, the value will be set to this property rather than using the db field name.
     *  * toDbValue: callable. If set this callable will be used when casting the value as a string.
     *  * rawDataContainer. The name of the rawData property which holds the values for this item
     * @return array
     */
    public static function getDbFields(): array;

    /**
     * Get The table field containing the Model's unique id
     *
     * @return string
     */
    public static function getDbPrimaryKeyField(): string;

    /**
     * @return class-string<CoreModelCollectionInterface>
     */
    public static function getCollectionModel(): string;

    /**
     * Get or establish the one-to-many relations config
     *
     * Once the relation has been established with a model and a foreign key,
     * then only the reference is needed to return the relation.
     *
     * An established relation cannot be overwritten, but a new relation can be established with a new reference.
     *
     * All default references should be established at the very first call
     * (by checking if self::$one2Many is still null)
     *
     * @param string $reference
     * @param class-string<CoreModelInterface>|null $model The model which is linked to this model (required when setting up new relation)
     * @param string|null $foreignKey The column in the related item's table which points back to this model (required when setting up new relation)
     * @param array $filters Any filters in addition to the foreign key match
     * @param callable|null $callback Optional callback for setting sort order etc of the related items.
     * The callback will be passed the CoreModelCollection object as the only argument
     * @return array{model: class-string<CoreModelInterface>, foreignTable: string, foreignKey: string, filters: array[]|string[], callback: callable|null}|null
     * Returns the one-2-many configuration if one is set, as an associative array,
     * containing `model`, `foreignTable`, `foreignKey`, `filters` and `callback`
     * @throws CoreModelException
     */
    public static function hasMany(
        string  $reference,
        ?string $model = null,
        ?string $foreignKey = null,
        array   $filters = [],
        ?callable $callback = null
    ): ?array;

    /**
     * Check if a property has been loaded and set
     *
     * @param string $property
     * @return bool
     */
    public function hasData(string $property): bool;

    /**
     * Get this object's properties
     *
     * @param string|null $property
     * @return mixed
     * @throws CoreModelException
     */
    public function getData(?string $property = null);

    /**
     * Cover (up) a property with a mock or real value,
     * to prevent it from loading the real value
     * This will set a read-only value to the model
     *
     * @param string $property
     * @param $value
     * @return CoreModelInterface
     */
    public function coverData(string $property, $value): self;

    /**
     * Set the value of a property and save to the database
     *
     * @param string $property
     * @param $value
     * @return CoreModelInterface
     * @throws CoreModelException
     */
    public function setData(string $property, $value): self;

    /**
     * Loads the database values into the rawData property for the corresponding table
     *
     * @throws CoreModelException
     */
    public function load(): self;

    /**
     * Get the data mapping for the core db table
     *
     * Returns the mapping from requested data field to the corresponding rawData / table field
     * The method returns an array of all the data properties as key,
     * and the corresponding table and field as table=>key keypair
     *
     * @return string[][]
     */
    public function getDataMapping(): array;

    /**
     * Get the data mapping for a specific property
     * The core data fields is searched first, and, if the property is not found there,
     * then the eav mappings are loaded and searched.
     * The result is returned as an array with one item,
     * holding the table name as key and the field name as value.
     * Null is returned if the property can't be found.
     *
     * @param string $property
     * @return string[]|null
     */
    public function getDataMappingForProperty(string $property): ?array;

    /**
     * Get the db source (table) for a property
     *
     * @param string $property
     * @return string|null
     */
    public function getDbSourceForProperty(string $property): ?string;

    /**
     * Get the database field for a property
     *
     * @param string $property
     * @return string|null
     */
    public function getDbFieldForProperty(string $property): ?string;

    /**
     * Get the EAV value mapping
     *
     * Adds any EAV mappings to the fields and dataMapping properties,
     * which map the requested data field to the corresponding rawData / table field
     *
     * @return array all the loaded mappings
     */
    public function getEavMapping(): array;

    /**
     * Get the Id
     *
     * @return mixed
     * @throws CoreModelException
     */
    public function getId();

    /**
     * Set the Id
     *
     * @param mixed $id
     * @return $this
     */
    public function setId($id): self;

    /**
     * Set preloaded values to the object
     *
     * @param stdClass $rawData
     * @return CoreModelInterface
     */
    public function setPreloadedValues(stdClass $rawData): self;

    /**
     * Set the raw data
     *
     * @param $source
     * @param $field
     * @param $value
     */
    public function setRawData($source, $field, $value);

    /**
     * Output the RawData
     *
     * (Unformatted data as it was loaded from the db)
     *
     * @return stdClass
     * @throws CoreModelException
     */
    public function getRawData(): stdClass;

    /**
     * Get the EAV configurations
     *
     * @return stdClass
     */
    public function getEavConfigs(): stdClass;

    /**
     * Get the EAV value objects
     *
     * @param string $source
     * @return null|stdClass
     */
    public function getEavValueObjects(string $source): ?stdClass;

    /**
     * Get the AppInterface instance
     *
     * @return AppInterface
     */
    public function getApp(): AppInterface;

    /**
     * @return bool
     */
    public function getForceReload(): bool;

    /**
     * @param bool $forceReload
     * @return $this
     */
    public function setForceReload(bool $forceReload): self;

    /**
     * Reset
     *
     * Resets this model instance, so it needs to be reloaded from database
     *
     * @return $this
     */
    public function reset(): self;

    /**
     * Create
     *
     * Creates a new model instance in the database
     * All param names should be given as underscore
     *
     * @param stdClass $params
     * @return CoreModelInterface
     * @throws CoreModelException
     */
    public function create(stdClass $params): self;

    /**
     * To Db Value
     *
     * Returns the value converted to a string or scalar that can be saved in a database
     * Conversion is done according to the specifications in $config
     *
     * @param stdClass $config
     * @param $value
     * @return mixed
     */
    public function toDbValue(stdClass $config, $value);

    /**
     * Delete this model object from the database
     *
     * @return void
     * @throws CoreModelException
     */
    public function delete();

    /**
     * Load EAV
     *
     * @return $this
     */
    public function loadEav(): self;

    /**
     * Get the value of a property as a string
     *
     * @param $property
     * @return string|null
     * @throws CoreModelException
     */
    public function getStringValue($property): ?string;

    /**
     * This method takes a set of property => value pairs and attempts to save these values directly to the db source
     *
     * @param array|stdClass $rawValues
     * @return CoreModelInterface
     * @throws CoreModelException
     */
    public function saveRawValues($rawValues): self;

    /**
     * Get a list of the EAV data sources
     * @return string[]
     */
    public function getEavSources(): array;

    /**
     * Returns the config for a db field used by this model as an object
     *
     * @param string $source
     * @param string $field
     * @return stdClass
     */
    public function getFieldConfig(string $source, string $field): stdClass;

    /**
     * Return a stdClass object with empty properties for each rawData attribute
     *
     * @param string $source
     * @return stdClass
     */
    public function getValuesPlaceholder(string $source): stdClass;

    /**
     * @return string[]
     */
    public function getPropertyNames(): array;

    /**
     * @return stdClass
     * @throws CoreModelException
     */
    public function getFlatData(): stdClass;

    /**
     * @param string $collection
     * @param array $collectionItems
     * @param bool $replaceExisting
     * @return $this
     */
    public function setPreloadCollectionItems(string $collection, array $collectionItems, bool $replaceExisting = false): self;

    /**
     * Return a collection of other models based on its one-2-many relation
     *
     * @param string $reference
     * @return CoreModelCollectionInterface|null
     */
    public function getCollection(string $reference):?CoreModelCollectionInterface;
}