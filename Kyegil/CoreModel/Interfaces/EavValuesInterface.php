<?php

namespace Kyegil\CoreModel\Interfaces;


/**
 * Interface EavValuesInterface
 * @package Kyegil\CoreModel\Interfaces
 */
interface EavValuesInterface extends CoreModelInterface
{
    /**
     * @return EavConfigInterface
     */
    public function getConfig(): EavConfigInterface;

    /**
     * @return void
     */
    public function delete();
}