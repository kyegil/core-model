<?php

namespace Kyegil\CoreModel\Interfaces;


/**
 * Interface EavConfigInterface
 * @package Kyegil\CoreModel\Interfaces
 */
interface EavConfigInterface extends CoreModelInterface

{
    /**
     * @return array
     */
    public function getConfiguration(): array;

    /**
     * @return string
     */
    public function getTarget(): string;

    /**
     * @return string
     */
    public function getSourceField(): string;

    /**
     * @return mixed
     */
    public function getDataType();

    /**
     * @return CoreModelCollectionInterface
     */
    public function getValuesCollection(): CoreModelCollectionInterface;

    /**
     * @return string
     */
    public function getEavCode(): string;

    /**
     * @return string
     */
    public function getEavValueField(): string;

    /**
     * @return string
     */
    public function getValuesModelName(): string ;

    /**
     * @return string
     */
    public function getValuesJoinField(): string ;

    /**
     * @return string
     */
    public function getConfigJoinField(): string ;

    /**
     * @return boolean
     */
    public function getAllowNull(): bool ;

    /**
     * @return boolean
     */
    public function getAllowMultiple(): bool ;

    /**
     * @param $value
     * @return $this
     */
    public function validateValue($value): EavConfigInterface;
}