<?php

namespace Kyegil\CoreModel\Interfaces;


/**
 * Interface CoreModelRepositoryInterface
 * @package Kyegil\CoreModel\Interfaces
 */
interface CoreModelRepositoryInterface
{
    /**
     * @param CoreModelInterface $object
     * @return CoreModelInterface
     */
    public function save(CoreModelInterface $object);

    /**
     * @param string $class
     * @param int|string $id
     * @return CoreModelInterface
     */
    public function get(string $class, $id);
}