<?php

namespace Kyegil\CoreModel;


use DateInterval;
use DateTimeInterface;
use DateTimeZone;
use Exception;
use Kyegil\CoreModel\Interfaces\AppInterface;
use Kyegil\CoreModel\Interfaces\CoreModelCollectionInterface;
use Kyegil\CoreModel\Interfaces\CoreModelInterface;
use Kyegil\CoreModel\Interfaces\CoreModelRepositoryInterface;
use Kyegil\CoreModel\Interfaces\EavConfigInterface;
use Kyegil\CoreModel\Interfaces\EavValuesInterface;
use mysqli;
use Psr\Log\LoggerInterface;
use stdClass;

/**
 * Class CoreModel
 * @package Kyegil\CoreModel
 */
class CoreModel implements CoreModelInterface
{
    /** @var string The db table storing the primary key for this object */
    protected static $dbTable;

    /** @var string The collection model to use for collections of this model */
    protected static $collectionModel = CoreModelCollection::class;

    /**
     * @var array|null
     */
    protected static ?array $one2Many;

    /**
     * @var LoggerInterface
     */
    public static $logger = null;
    /**
     * @var
     */
    public static $tablePrefix;

    /**
     * @var int
     */
    public static $queryCount = 0;

    /**
     * The 'fields' property contains the models tables and table fields.
     * It is populated by the model constructor, and has the following format:
     * $this->fields[ dbTable ][ dbField ][ config-array]
     *
     * @var array[][]
     */
    protected $fields = [];

    /** @var array*/
    protected $dataMapping;

    /** @var array */
    protected $eavConfigs;

    /** @var null|stdClass */
    protected $eavValueObjects;

    /** @var string The table field containing the primary key */
    protected static $dbPrimaryKeyField;

    /** @var AppInterface Property holding the App */
    protected $app;

    /** @var mysqli The MySQLi connection */
    protected $mysqli;

    /** @var stdClass Raw db values */
    protected $rawData;

    /** @var stdClass Internal cache for the db values */
    protected $data;

    /** @var stdClass Internal cache for the loaded collections */
    protected $collections;

    /** @var mixed Unique ID for this object */
    protected $id;

    /** @var CoreModelRepository */
    protected $coreModelRepository;

    /** @var bool */
    protected $forceReload = false;

    /**
     * Get the name of the db table storing this model's instances
     *
     * @return string
     */
    public static function getDbTable(): string
    {
        return static::$dbTable;
    }

    /**
     * Get Db fields
     *
     * Returns an associated array with the fields of the main object table as key,
     * and an associated array as configuration for each of the fields
     *
     * Possible config options:
     * * type: The data type. Defaults to 'string'. 'string'|'int'|'bool'|'date'|'time'|'datetime'|'interval'|'json',
     *      the name of a \Kyegil\CoreModel\Interfaces\CoreModelInterface class,
     *      or a callable casting the string value in the correct type
     *  * allowNull: boolean. Defaults to true. False will disallow null values
     *  * allowMultiple: boolean. Defaults to false. True will allow multiple instances of the chosen type.
     *      Use with care, as this will complicate table joining.
     *  * target: string. If set, the value will be set to this property rather than using the db field name.
     *  * toDbValue: callable. If set this callable will be used when casting the value as a string.
     *  * rawDataContainer. The name of the rawData property which holds the values for this item
     * @return array
     */
    public static function getDbFields(): array
    {
        return [];
    }

    /**
     * @param string $dbTable
     */
    public static function setDbTable(string $dbTable): void
    {
        static::$dbTable = $dbTable;
    }

    /**
     * @param DateInterval|null $dateInterval
     * @return string|null
     */
    public static function prepareIso8601IntervalSpecification(?DateInterval $dateInterval = null): ?string
    {
        if (!$dateInterval) {
            return null;
        }
        if (!$dateInterval->y && !$dateInterval->m && !$dateInterval->d && !$dateInterval->h && !$dateInterval->i && !$dateInterval->s) {
            $intervalSpecification = 'P0M';
        } else {
            $intervalSpecification = "P"
                . ($dateInterval->invert ? "-" : "")
                . ($dateInterval->y ? "{$dateInterval->y}Y" : "")
                . ($dateInterval->m ? "{$dateInterval->m}M" : "")
                . ($dateInterval->d ? "{$dateInterval->d}D" : "")
                . (($dateInterval->h || $dateInterval->i || $dateInterval->s) ? "T" : "")
                . ($dateInterval->h ? "{$dateInterval->h}H" : "")
                . ($dateInterval->i ? "{$dateInterval->i}M" : "")
                . ($dateInterval->s ? "{$dateInterval->s}S" : "");
        }
        return $intervalSpecification;
    }

    /**
     * Get The table field containing the Model's unique id
     *
     * @return string
     */
    public static function getDbPrimaryKeyField(): string
    {
        return static::$dbPrimaryKeyField;
    }

    /**
     * @param string $dbPrimaryKeyField
     */
    public static function setDbPrimaryKeyField(string $dbPrimaryKeyField): void
    {
        static::$dbPrimaryKeyField = $dbPrimaryKeyField;
    }

    /**
     * @return class-string<CoreModelCollectionInterface>
     */
    public static function getCollectionModel(): string {
        return static::$collectionModel;
    }

    /**
     * @param string $collectionModel
     */
    public static function setCollectionModel(string $collectionModel): void {
        static::$collectionModel = $collectionModel;
    }

    /**
     * Get or establish the one-to-many relations config
     *
     * Once the relation has been established with a model and a foreign key,
     * then only the reference is needed to return the relation.
     *
     * An established relation cannot be overwritten, but a new relation can be established with a new reference.
     *
     * All default references should be established at the very first call
     * (by checking if self::$one2Many is still null)
     *
     * @param string $reference
     * @param class-string<CoreModelInterface>|null $model The model which is linked to this model (required when setting up new relation)
     * @param string|null $foreignKey The column in the related item's table which points back to this model (required when setting up new relation)
     * @param array $filters Any filters in addition to the foreign key match
     * @param callable|null $callback Optional callback for setting sort order etc of the related items.
     * The callback will be passed the CoreModelCollection object as the only argument
     * @return array{model: class-string<CoreModelInterface>, foreignTable: string, foreignKey: string, filters: array[]|string[], callback: callable|null}|null
     * Returns the one-2-many configuration if one is set, as an associative array,
     * containing `model`, `foreignTable`, `foreignKey`, `filters` and `callback`
     * @throws CoreModelException
     */
    public static function hasMany(
        string  $reference,
        ?string $model = null,
        ?string $foreignKey = null,
        array   $filters = [],
        ?callable $callback = null
    ): ?array {
        settype(static::$one2Many, 'array');
        if(empty(static::$one2Many[$reference])) {
            if(!is_a($model, CoreModelInterface::class, true) || empty($foreignKey)) {
                throw new CoreModelException('Foreign model relation not configured');
            }
            static::$one2Many[$reference] = [
                'model' => $model,
                'foreignTable' =>  $model::getDbTable(),
                'foreignKey' => $foreignKey,
                'filters' => $filters,
                'callback' => $callback
            ];
        }
        $model = static::$one2Many[$reference]['model'];
        if(!isset(static::$one2Many[$reference]['foreignTable'])) {
            static::$one2Many[$reference]['foreignTable'] = $model::getDbTable();
        }
        return static::$one2Many[$reference] ?? null;
    }

    /**
     * @param string $message
     * @param string $level
     * @param array $context
     * @return void
     */
    public static function log(string $message, string $level = 'debug', array $context = [])
    {
        if(self::$logger instanceof LoggerInterface) {
            self::$logger->$level($message, $context);
        }
    }

    /**
     * @param string $message
     * @param array $context
     * @return void
     */
    public static function logError(string $message, array $context = [])
    {
        if(self::$logger instanceof LoggerInterface) {
            self::$logger->error($message, $context);
        }
    }

    /**
     * @param stdClass $item
     * @return object
     */
    public static function mapNamespacedRawData(stdClass $item): object
    {
        $coreData = array_filter((array)$item, function($property){
            return !strpos($property, '.');
        }, ARRAY_FILTER_USE_KEY);
        $rawData = (object)[static::getDbTable() => (object)$coreData];
        $additionalData = array_filter((array)$item, function($property){
            return strpos($property, '.');
        }, ARRAY_FILTER_USE_KEY);
        foreach ($additionalData as $property => $value) {
            $property = explode('.', $property, 2);
            settype($rawData->{$property[0]}, 'object');
            $rawData->{$property[0]}->{$property[1]} = $value;
        }

        return $rawData;
    }

    /**
     * Sets the data value for a requested field,
     * based on the rawdata value, and the field configurations
     *
     * @param array $field The rawData source (table) property and field property, given as source => field
     * @return self
     * @throws Exception
     */
    private function mapField(array $field): CoreModelInterface
    {
        $source = array_keys($field)[0];
        $field = reset($field);

        $coreSource = static::getDbTable();

        if(!key_exists($field, $this->fields[$source])) {
            throw new CoreModelException("Trying to map non-configured attribute {$field} of " . get_class($this));
        }
        $config = $this->fields[$source][$field];
        if(!isset($config['type'])) {
            $config['type'] = 'string';
        }
        $target = $config['target'] ?? $field;

        if(!isset($this->rawData->{$source}) || !property_exists($this->rawData->{$source}, $field)) {
            if($source == $coreSource) {
                $this->load();
            }
            else {
                // The field was not found in the model's core source
                // Now load any eav sources to check there
                $eavSources = $this->getEavSources();
                if(in_array($source, $eavSources)) {
                    $this->loadEav();
                }
            }
        }

        if(isset($this->rawData->{$source}) && property_exists($this->rawData->{$source}, $field)) {
            $this->data->$target = $this->rawData->{$source}->$field;

            if(
                $this->data->$target === null
                && (
                    ((isset($config['allowNull']) && $config['allowNull']))
                    || $field == static::$dbPrimaryKeyField
                )
            ) {
                $this->data->$target = null;
            }
            else {
                $this->data->$target = $this->castFromString($this->data->$target, (object)$config);
            }
        }

        return $this;
    }

    /**
     * @param $value
     * @param string|callable $type
     * @return string
     */
    private function castAsString($value, $type): string
    {
        $utc = new DateTimeZone('UTC');
        switch ($type) {
            case 'json':
                return json_encode($value);
            case 'datetime':
                if ($value instanceof DateTimeInterface) {
                    $value = $value->setTimezone($utc);
                    return $value->format('Y-m-d H:i:s');
                }
                break;
            case 'date':
                if ($value instanceof DateTimeInterface) {
                    /** The timezone should be ignored on dates without time element */
                    return $value->format('Y-m-d');
                }
                break;
            case 'time':
                if ($value instanceof DateTimeInterface) {
                    $value->setTimezone($utc);
                    return $value->format('H:i:s');
                }
                break;
            case 'interval':
                if ($value instanceof DateInterval) {
                    return self::prepareIso8601IntervalSpecification($value);
                }
                break;
            case 'bool':
            case 'boolean':
                return $value ? '1' : '0';
        }
        return (string)$value;
    }

    /**
     * @param string|null $value
     * @param object $config
     * @return mixed
     * @throws Exception
     */
    private function castFromString(?string $value, object $config)
    {
        $type = $config->type ?? 'string';
        $utc = new DateTimeZone('UTC');
        $allowMultiple = !empty($config->allowMultiple);

        try {
            if (is_a($type, CoreModelInterface::class, true)) {
                $value = $this->castCoreModelFromString($value, $config);
            }
            elseif ($type == 'json') {
                $value = json_decode($value);
            }
            elseif(in_array($type, ['datetime', 'date', 'time', 'interval', 'int', 'integer', 'string', 'bool', 'boolean'])) {
                if($allowMultiple) {
                    $subConfig = clone $config;
                    $subConfig->allowMultiple = false;
                    $values = $type == 'string'
                        ? json_decode('[' . $value . ']')
                        : explode(',', $value);
                    ;
                    $value = array_map(function($value) use ($subConfig) {
                        return $this->castFromString($value, $subConfig);
                    }, $values);
                }
                elseif ($type == 'datetime') {
                    $value = date_create($value, $utc);
                }
                elseif ($type == 'date') {
                    /** The timezone should be ignored on dates without time element */
                    $value = date_create_from_format('Y-m-d', $value) ?: null;
                    if($value instanceof DateTimeInterface) {
                        $value->settime(0, 0);
                    }
                }
                elseif ($type == 'time') {
                    $value = date_create_from_format('H:i:s', $value, $utc) ?: null;
                }
                elseif ($type == 'interval' && $value) {
                    $value = new DateInterval($value);
                }
                else {
                    settype($value, $type);
                }
            }
            elseif (is_callable($type)) {
                $value = $type($value, $this);
            }
        }
        catch (Exception $e) {
            self::logError($e->getMessage(), $e->getTrace());
        }
        return $value;
    }

    /**
     * @param string|null $value
     * @param object $config
     * @return CoreModelCollection|CoreModel|null
     * @throws CoreModelException
     */
    private function castCoreModelFromString(?string $value, object $config)
    {
        if($value !== null || empty($config->allowNull)) {
            if(empty($config->allowMultiple)) {
                /** @var CoreModel $value */
                $value = $this->app->getModel($config->type, $value);
                $rawDataProperty = $config->rawDataContainer ?? $value::getDbTable();
                if(property_exists($this->rawData, $rawDataProperty)) {
                    $primaryKeyField = $value::getDbPrimaryKeyField();
                    $preloadedValues = $value->mapNamespacedRawData($this->rawData->$rawDataProperty);
                    if(isset($this->rawData->$rawDataProperty->$primaryKeyField)) {
                        $value->setPreloadedValues($preloadedValues);
                        $this->coreModelRepository->save($value);
                    }
                }
            }
            else {
                $value = $this->app->getModelCollection($config->type)
                    ->filterByItemIds(explode(',', $value))
                    ->lockFilters();
            }
        }
        return $value;
    }

    /**
     * Prepare all data on the data property according to the mapping given in self::getDbFields
     *
     * @param string|null $requestedProperty
     * @return CoreModel
     * @throws CoreModelException|Exception
     */
    protected function prepareData(?string $requestedProperty = null): CoreModelInterface
    {
        list('requestedProperty' => $requestedProperty)
            = $this->app->before($this, __FUNCTION__, [
            'requestedProperty' => $requestedProperty
        ]);
        $mapping = $this->getDataMapping();

        if($requestedProperty === null) {
            // We get mapping for all the fields, so we need to also load the eav mapping
            $mapping = $this->getEavMapping();
            foreach($mapping as $property => $source) {
                if(!property_exists($this->data, $property)) {
                    $this->mapField($source);
                }
            }
        }

        else {
            // The request is for a specific field that exists in the mapping
            if(!isset($mapping[$requestedProperty])) {
                // The mapping could not be found in the core mapping. It's time to load the eav mapping
                $mapping = $this->getEavMapping();
            }
            if(isset($mapping[$requestedProperty])) {
                $this->mapField($mapping[$requestedProperty]);
            }
        }
        return $this;
    }

    /**
     * Save value
     *
     * @param string $source
     * @param string $field
     * @param $value
     * @return self
     * @throws CoreModelException
     */
    protected function saveValue(string $source, string $field, $value): CoreModelInterface
    {
        $eavSources = $this->getEavSources();
        if(in_array($source, $eavSources)) {
            $this->saveEavValue($source, $field, $value);
            $this->data = new stdClass();
            return $this;
        }

        $fieldConfig = $this->fields[$source][$field];
        $dbValue = $this->toDbValue((object)$fieldConfig, $value);

        $this->saveToMainTable([$field => $dbValue]);
        $this->reset();
        return $this;
    }

    /**
     * Save the EAV value
     *
     * @param string $source
     * @param string $field
     * @param $value
     * @return self|EavValuesInterface
     * @throws CoreModelException
     */
    protected function saveEavValue(string $source, string $field, $value): CoreModelInterface
    {
        list('source' => $source, 'field' => $field, 'value' => $value)
            = $this->app->before($this, __FUNCTION__, [
            'source' => $source, 'field' => $field, 'value' => $value
        ]);
        $eavConfigs = $this->getEavConfigs();
        /** @var EavConfigInterface $configAttribute */
        $configAttribute = $eavConfigs->{$source}->{$field};

        $configAttribute->validateValue($value);

        $fieldConfig = $this->fields[$source][$field];
        $dbValue = $this->toDbValue((object)$fieldConfig, $value);

        $result = $this->updateRawEavValue($source, $field, $dbValue);
        return $this->app->after($this, __FUNCTION__, $result);
    }

    /**
     * Update the Raw EAV value
     *
     * @param string $source
     * @param string $field
     * @param $dbValue
     * @return CoreModel
     * @throws CoreModelException
     */
    protected function updateRawEavValue(string $source, string $field, $dbValue): CoreModelInterface
    {
        $eavConfigs = $this->getEavConfigs();
        if(!isset($eavConfigs->{$source})) {
            throw new CoreModelException("Config for {$source} attribute is missing");
        }
        $eavValueObjects = $this->getEavValueObjects($source);
        if($eavValueObjects instanceof stdClass && property_exists($eavValueObjects, $field)) {
            /** @var EavValuesInterface $eavValueObject */
            $eavValueObject = $eavValueObjects->{$field};
            $valueField = $eavValueObject->getConfig()->getEavValueField();

            $eavValueObject->setData($valueField, $dbValue);

            if($dbValue === null) {
                $eavValueObject->delete();
                unset($this->eavValueObjects->$source->$field);
            }
        }
        elseif($dbValue !== null) {
            $this->createEavValue($source, $field, $dbValue);
        }
        settype($this->rawData->{$source}, 'object');
        $this->rawData->{$source}->{$field} = $dbValue;
        return $this;
    }

    /**
     * Create EAV value
     *
     * @param string $source
     * @param string $field
     * @param $value
     * @return EavValuesInterface
     * @throws CoreModelException
     */
    protected function createEavValue(string $source, string $field, $value): EavValuesInterface
    {
        $eavConfigs = $this->getEavConfigs();
        if(!isset($eavConfigs->{$source})) {
            throw new CoreModelException("Config for {$source} attribute is missing");
        }
        /** @var EavConfigInterface $config */
        $config = $eavConfigs->{$source}->{$field};
        $model = $config->getValuesModelName();
        $thisJoinField = $config->getValuesJoinField();
        $configJoinField = $config->getConfigJoinField();
        $valueField = $config->getEavValueField();

        /** @var EavValuesInterface $valueObject */
        $valueObject = $this->app->newModel($model, (object)[
            $thisJoinField => $this->getId(),
            $configJoinField => $config->getId(),
            $valueField => $value
        ]);
        settype($this->eavValueObjects->{$source}, 'object');
        $this->eavValueObjects->{$source}->{$config->getSourceField()} = $valueObject;
        return $valueObject;
    }

    /**
     * Split Properties by db source
     *
     * This method takes an object with property => value
     * and splits it into several objects grouped by db source
     *
     * @param array|stdClass $properties
     * @return stdClass An object with one property per source, holding an object with the original properties
     */
    protected function splitPropertiesBySource($properties): stdClass {
        settype($properties, 'object');
        $result = (object)[
            static::getDbTable() => (object)[]
        ];
        foreach($properties as $property => $value) {
            $source = $this->getDbSourceForProperty($property);
            if($source) {
                settype($result->$source, 'object');
                $result->$source->$property = $value;
            }
        }
        return $result;
    }

    /**
     * Save string values straight to the main table
     *
     * @param array|stdClass $values
     * @return self
     * @throws CoreModelException
     */
    protected function saveToMainTable($values): CoreModelInterface
    {
        $source = static::getDbTable();
        $tp = self::$tablePrefix ?? '';
        $bindValues = [];
        $update = (bool)$this->getId();
        if ($update) {
            $fields = [];
            $updateSql = "UPDATE `{$tp}{$source}`\n";

            foreach ($values as $key => $value) {
                if (is_null($value)) {
                    $fields[] = "`$key` = NULL";
                }
                else {
                    $fields[] = "`$key` = ?";
                    $bindValues[] = $value;
                }
            }
            $set = 'SET ' . implode(', ', $fields) . "\n";
            $where = 'WHERE `' . static::getDbPrimaryKeyField() . '` = ?';
            $bindValues[] = $this->getId();
            $sql = $updateSql . $set . $where;
            $statement = $this->mysqli->prepare($sql);
            if (!$statement) {
                throw new CoreModelException($this->mysqli->error);
            }

            if ($bindValues) {
                $types = str_repeat('s', count($bindValues));
                $statement->bind_param($types, ...$bindValues);
            }
            CoreModel::log($sql, 'debug', $bindValues);
            if (!$statement->execute()) {
                throw new CoreModelException($statement->error);
            }
            CoreModel::$queryCount++;
        }
        else {
            $fields = [];
            $insertSql = "INSERT INTO\n{$tp}{$source}";

            foreach ($values as $key => $value) {
                if (is_null($value)) {
                    $fields[] = "$key = NULL\n";
                }
                else {
                    $fields[] = "$key = ?\n";
                    $bindValues[] = $value;
                }
            }
            $set = "SET\n" . implode(', ', $fields);
            $sql = $insertSql . $set;
            $statement = $this->mysqli->prepare($sql);
            if (!$statement) {
                throw new CoreModelException($this->mysqli->error);
            }

            if ($bindValues) {
                $types = str_repeat('s', count($bindValues));
                $statement->bind_param($types, ...$bindValues);
            }
            CoreModel::log($sql, 'debug', $bindValues);
            if (!$statement->execute()) {
                throw new CoreModelException($statement->error);
            }
            CoreModel::$queryCount++;
            $this->setId($this->mysqli->insert_id);
        }
        return $this->reset();
    }

    /**
     * CoreModel constructor.
     *
     * Default constructor for all child classes.
     * @param array $di Dependency Injection of classes and Interfaces
     * @param int|string $id id-value as stored in the database
     * @throws CoreModelDiException
     */
    public function __construct(array $di, $id = null ) {
        $diRequired = [
            AppInterface::class,
            mysqli::class,
            CoreModelRepositoryInterface::class
        ];

        foreach($diRequired as $class) {
            if(!isset($di[$class]) || !is_a($di[$class], $class)) {
                throw new CoreModelDiException("Required instance of " . $class . " has not been injected to " . static::class . " constructor");
            }
        }
        $this->app = $di[AppInterface::class];
        $this->mysqli = $di[mysqli::class];
        $this->coreModelRepository = $di[CoreModelRepositoryInterface::class];

        $this->id = $id;

        $this->fields[static::getDbTable()] = static::getDbFields();
        $this->reset();
        $this->forceReload = false;
    }

    /**
     * @param $name
     * @param $arguments
     * @return mixed
     * @throws CoreModelException
     */
    public function __call($name, $arguments)
    {
        $underscore = $this->app->makeUnderscore($name);
        if(substr($underscore, 0,4) == 'get_') {
            $attribute = substr($underscore, 4);

            list('attribute' => $attribute)
                = $this->app->before($this, $name, ['attribute' => $attribute]);
            $result = $this->getData($attribute);
        }
        elseif(substr($underscore, 0,4) == 'set_') {
            $attribute = substr($underscore, 4);
            if(count($arguments) == 0) {
                throw new CoreModelException($name . '() requires a value');
            }
            list('value' => $arguments)
                = $this->app->before($this, $name, ['value' => $arguments]);

            $result = $this->setData($attribute, reset($arguments));
        }
        else{
            throw new CoreModelException('Unknown method ' . static::class . '::' . $name . '()');
            }
        return $this->app->after($this, $name, $result);
    }

    /**
     * @param $name
     * @return mixed
     * @throws CoreModelException
     */
    public function __get($name)
    {
        $getter = 'get' . ucfirst($name);
        return $this->$getter();
    }

    /**
     * @param $name
     * @param $value
     * @return CoreModel
     * @throws CoreModelException
     */
    public function __set($name, $value)
    {
        $setter = 'set' . ucfirst($name);
        return $this->$setter($value);
    }

    /**
     * Textual representation of the object will be its id
     *
     * @return string
     * @throws CoreModelException
     */
    public function __toString() {
        return (string)$this->getId();
    }

    /**
     * Check if a property has been loaded and set
     *
     * @param string $property
     * @return bool
     */
    public function hasData(string $property): bool
    {
        list('property' => $property)
            = $this->app->before($this, __FUNCTION__, ['property' => $property]);
        return $this->app->after($this, __FUNCTION__, property_exists($this->data, $property));
    }

    /**
     * Get this object's properties
     *
     * @param string|null $property
     * @return mixed
     * @throws CoreModelException
     */
    public function getData(?string $property = null)
    {
        list('property' => $property)
            = $this->app->before($this, __FUNCTION__, [
            'property' => $property
        ]);
        if($property === null || !property_exists($this->data, $property)) {
            $this->prepareData($property);
        }
        if($property === null) {
            $result = $this->data;
        }
        else {
            $result = property_exists($this->data, $property) ? $this->data->$property : null;
        }
        return $this->app->after($this, __FUNCTION__, $result);
    }

    /**
     * Cover (up) a property with a mock or real value,
     * to prevent it from loading the real value
     * This will set a read-only value to the model
     *
     * @param string $property
     * @param $value
     * @return CoreModel
     */
    public function coverData(string $property, $value): CoreModelInterface
    {
        list('property' => $property, 'value' => $value)
            = $this->app->before($this, __FUNCTION__, [
            'property' => $property, 'value' => $value
        ]);
        $this->data->$property = $value;
        return $this->app->after($this, __FUNCTION__, $this);
    }

    /**
     * Set the value of a property and save to the database
     *
     * @param string $property
     * @param $value
     * @return CoreModel
     * @throws CoreModelException
     */
    public function setData(string $property, $value): CoreModelInterface
    {
        list('property' => $property, 'value' => $value)
            = $this->app->before($this, __FUNCTION__, [
            'property' => $property, 'value' => $value
        ]);
        // Never unset the primary key
        if($property != static::getDbPrimaryKeyField()
            || !empty($value)
        ) {
            $this->getDataMapping();
            $mapping = $this->getEavMapping();
            /** @var array|false $field if the table and field exists in the mapping, it is as a key=>value pair */
            $field = $mapping[$property] ?? false;
            if($field) {
                $source = array_keys($field)[0];
                $field = reset($field);
                $this->saveValue($source, $field, $value);
            }
        }
        return $this->app->after($this, __FUNCTION__, $this);
    }

    /**
     * Loads the database values into the rawData property for the corresponding table
     *
     * @throws CoreModelException
     */
    public function load(): CoreModelInterface
    {
        $tp = self::$tablePrefix ?? '';
        try {
            $select = "SELECT\n`" . implode('`, `', array_keys($this->fields[static::getDbTable()])) . "`\n";
            $from = 'FROM ' . $tp . static::getDbTable() . ' AS ' . static::getDbTable() . "\n";
            $where = 'WHERE ' . static::getDbPrimaryKeyField() . ' = ?';
            $sql = $select . $from . $where;
            $statement = $this->mysqli->prepare($sql);
            if (!$statement) {
                throw new CoreModelException($this->mysqli->error);
            }
            $idType = $this->fields[static::getDbTable()][static::getDbPrimaryKeyField()]['type'] ?? 'string';
            $statement->bind_param(in_array($idType, ['int', 'integer']) ? 'i' : 's', $this->id);
            CoreModel::log($sql, 'debug', [$this->id]);
            $statement->execute();
            CoreModel::$queryCount++;
            $resultSet = $statement->get_result();
            if (!$resultSet) {
                throw new CoreModelException($this->mysqli->error);
            }
            $data = $resultSet->fetch_object();
            $resultSet->close();
            $statement->close();
        } catch (Exception $e) {
            self::logError($e->getMessage(), $e->getTrace());
            $this->forceReload = false;
            $this->coreModelRepository->remove(static::class, $this->id);
            throw new CoreModelException('Could not load ' . get_class($this) . ' (' . $this->id . ')');
        }
        if ($data) {
            $this->rawData->{static::getDbTable()} = $data;
            $this->coreModelRepository->save($this);
        }
        return $this;
    }

    /**
     * Get the data mapping for the core db table
     *
     * Returns the mapping from requested data field to the corresponding rawData / table field
     * The method returns an array of all the data properties as key,
     * and the corresponding table and field as table=>key keypair
     *
     * @return string[][]
     */
    public function getDataMapping(): array
    {
        if($this->dataMapping === null) {
            $this->dataMapping = [];
            foreach($this->fields[static::getDbTable()] as $field => $mapping) {
                $target = $mapping['target'] ?? $field;
                $this->dataMapping[$target] = [static::$dbTable => $field];
            }
        }
        return $this->dataMapping;
    }

    /**
     * Get the data mapping for a specific property
     * The core data fields is searched first, and, if the property is not found there,
     * then the eav mappings are loaded and searched.
     * The result is returned as an array with one item,
     * holding the table name as key and the field name as value.
     * Null is returned if the property can't be found.
     *
     * @param string $property
     * @return string[]|null
     */
    public function getDataMappingForProperty(string $property): ?array
    {
        list('property' => $property)
            = $this->app->before($this, __FUNCTION__, [
            'property' => $property
        ]);
        $mapping = $this->getDataMapping();
        if(key_exists($property, $mapping)) {
            $result = $mapping[$property];
            return $this->app->after($this, __FUNCTION__, $result);
        }
        $mapping = $this->getEavMapping();
        if(key_exists($property, $mapping)) {
            $result = $mapping[$property];
            return $this->app->after($this, __FUNCTION__, $result);
        }
        return $this->app->after($this, __FUNCTION__, null);
    }

    /**
     * Get the db source (table) for a property
     *
     * @param string $property
     * @return string|null
     */
    public function getDbSourceForProperty(string $property): ?string
    {
        $mapping = $this->getDataMappingForProperty($property);
        if($mapping) {
            foreach($mapping as $key => $void) {
                return $key;
            }
        }
        return null;
    }

    /**
     * Get the database field for a property
     *
     * @param string $property
     * @return string|null
     */
    public function getDbFieldForProperty(string $property): ?string
    {
        $mapping = $this->getDataMappingForProperty($property);
        if($mapping) {
            return reset($mapping);
        }
        return null;
    }

    /**
     * Get the EAV value mapping
     *
     * Adds any EAV mappings to the fields and dataMapping properties,
     * which map the requested data field to the corresponding rawData / table field
     *
     * @return array all the loaded mappings
     */
    public function getEavMapping(): array
    {
        /**
         * @var string $source
         * @var CoreModelCollectionInterface $configCollection
         */
        foreach($this->getEavConfigs() as $source => $eavConfigs) {
            if(!isset($this->fields[$source])) {
                /** @var EavConfigInterface $eavConfig */
                foreach ($eavConfigs as $eavConfig) {
                    $configuration = $eavConfig->getConfiguration();
                    $target = $eavConfig->getTarget();
                    $field = $eavConfig->getSourceField();
                    $type = $eavConfig->getDataType();
                    $allowNull = $eavConfig->getAllowNull();
                    $allowMultiple = $eavConfig->getAllowMultiple();
                    $this->dataMapping[$target] = [$source => $field];
                    $this->fields[$source][$field] = array_merge($configuration, [
                        'type' => $type,
                        'target' => $target,
                        'allowNull' => $allowNull,
                        'allowMultiple' => $allowMultiple
                    ]);
                }
                settype($this->rawData->$source, 'object');
            }
        }
        return $this->dataMapping;
    }

    /**
     * Get the Id
     *
     * @return mixed
     * @throws CoreModelException
     */
    public function getId()
    {
        $primaryKey = static::$dbPrimaryKeyField;
        $config = $this->fields[static::$dbTable][$primaryKey];
        $target = $config['target'] ?? $primaryKey;
        $this->id = $this->getData($target);
        return $this->app->after($this, __FUNCTION__, $this->id);
    }

    /**
     * Set the Id
     *
     * @param mixed $id
     * @return self
     */
    public function setId($id): CoreModelInterface
    {
        $this->id = $id;
        return $this;
    }

    /**
     * Set preloaded values to the object
     *
     * @param stdClass $rawData
     * @return CoreModel
     */
    public function setPreloadedValues(stdClass $rawData): CoreModelInterface
    {
        if (!$this->forceReload) {
            $this->rawData = $rawData;
        }
        return $this;
    }

    /**
     * Set the raw data
     *
     * @param $source
     * @param $field
     * @param $value
     */
    public function setRawData($source, $field, $value)
    {
        settype($this->rawData->$source, 'object');
        $this->rawData->$source->$field = $value;
    }

    /**
     * Output the RawData
     *
     * (Unformatted data as it was loaded from the db)
     *
     * @return stdClass
     * @throws CoreModelException
     */
    public function getRawData(): stdClass
    {
        if(!property_exists($this->rawData, static::getDbTable())) {
            $this->load();
        }
        return $this->rawData;
    }

    /**
     * Get the EAV configurations
     *
     * @return stdClass
     */
    public function getEavConfigs(): stdClass
    {
        if(!$this->eavConfigs) {
            $this->eavConfigs = new stdClass();
        }
        return $this->eavConfigs;
    }

    /**
     * Get the EAV value objects
     *
     * @param string $source
     * @return null|stdClass
     */
    public function getEavValueObjects(string $source): ?stdClass
    {
        if(!$this->eavValueObjects) {
            $this->eavValueObjects = new stdClass();
        }
        if(isset($this->eavValueObjects->{$source})) {
            return $this->eavValueObjects->{$source};
        }
        return null;
    }

    /**
     * Get the AppInterface instance
     *
     * @return AppInterface
     */
    public function getApp(): AppInterface
    {
        return $this->app;
    }

    /**
     * @return bool
     */
    public function getForceReload(): bool
    {
        return $this->forceReload;
    }

    /**
     * @param bool $forceReload
     * @return self
     */
    public function setForceReload(bool $forceReload): CoreModelInterface
    {
        $this->forceReload = $forceReload;
        return $this;
    }

    /**
     * Reset
     *
     * Resets this model instance, so it needs to be reloaded from database
     *
     * @return self
     */
    public function reset(): CoreModelInterface
    {
        $this->rawData = new stdClass;
        $this->data = new stdClass();
        $this->collections = new stdClass();
        $this->forceReload = true;
        return $this;
    }

    /**
     * Create
     *
     * Creates a new model instance in the database
     * All param names should be given as underscore
     *
     * @param stdClass $params
     * @return self
     * @throws CoreModelException
     */
    public function create(stdClass $params): CoreModelInterface
    {
        $params = clone $params;

        list('params' => $params)
            = $this->app->before($this, __FUNCTION__, [
            'params' => $params
        ]);
        // The Id must never be set to null
        if(empty($params->{static::getDbPrimaryKeyField()})) {
            unset($params->{static::getDbPrimaryKeyField()});
        }
        // If the id field hase been set explicitly, then we'll save it here
        // to use after the insertion instead of any auto increment value
        $id = $params->{static::getDbPrimaryKeyField()} ?? null;
        $this->reset();
        if($this->getId()) {
            throw new CoreModelException('New model is not empty');
        }

        $tp = self::$tablePrefix ?? '';
        $table = static::getDbTable();
        $mapping = $this->getDataMapping();
        $insert = "INSERT INTO `{$tp}{$table}`";
        $bindValues = [];
        $values = [];

        foreach($params as $property => $value) {
            $field = $mapping[$property] ?? false;
            if($field && array_keys($field)[0] == $table) {
                $field = reset($field);
                $fieldConfig = $this->fields[$table][$field];
                $dbValue = $this->toDbValue((object)$fieldConfig, $value);

                $values[] = $field;
                $bindValues[] = $dbValue;
                unset($params->{$property});
            }
        }
        if ($values) {
            $insert .= "(`" . implode('`, `', $values) . "`)\n";
            $insert .= "VALUES (" . str_repeat('?, ', count($values) -1) . "?)\n";
        }
        else {
            $insert .= "() VALUES ()\n";
        }
        $statement = $this->mysqli->prepare($insert);
        if (!$statement) {
            throw new CoreModelException($this->mysqli->error);
        }
        if($bindValues) {
            $types = str_repeat('s', count($bindValues));
            $statement->bind_param($types, ...$bindValues);
        }
        CoreModel::log($insert, 'debug', $bindValues);
        if (!$statement->execute()) {
            throw new CoreModelException($statement->error);
        }
        $statement->close();
        CoreModel::$queryCount++;
        $this->setId($id ?: $this->mysqli->insert_id);
        $this->reset();

        foreach($params as $property => $value) {
            $this->setData($property, $value);
        }

        return $this->app->after($this, __FUNCTION__, $this);
    }

    /**
     * To Db Value
     *
     * Returns the value converted to a string or scalar that can be saved in a database
     * Conversion is done according to the specifications in $config
     *
     * @param stdClass $config
     * @param $value
     * @return mixed
     * @throws CoreModelException
     */
    public function toDbValue(stdClass $config, $value)
    {
        list('config' => $config, 'value' => $value)
            = $this->app->before($this, __FUNCTION__, [
            'config' => $config, 'value' => $value
        ]);
        if(isset($config->toDbValue) && is_callable($config->toDbValue)) {
            return $config->toDbValue->__invoke($value);
        }
        $type = $config->type ?? 'string';
        settype($config->allowNull, 'bool');
        if($value === '' && $config->allowNull) {
            $value = null;
        }
        if($value !== null || !$config->allowNull) {

            /*
             * Saving CoreModel Collections
             */
            if(
                !empty($config->allowMultiple)
                && $value instanceof CoreModelCollectionInterface
                && is_a($value->getModel(), $type, true)
            ) {
                $value = implode(',', $value->getItemIds());
            }

            else if (!empty($config->allowMultiple)) {
                $values = is_array($value) ? $value : [$value];
                $values = array_map(function($value) use ($type) {
                    return $this->castAsString($value, $type);
                }, $values);
                $value = $type == 'string'
                    ? substr(json_encode(',', $values), 1, -1)
                    : implode(',', $values);
            }

            else {
                $value = $this->castAsString($value, $type);
            }
        }
        return $this->app->after($this, __FUNCTION__, $value);
    }

    /**
     * Delete this model object from the database
     *
     * @return void
     * @throws CoreModelException
     */
    public function delete()
    {
        $this->app->before($this, __FUNCTION__, []);
        $tp = self::$tablePrefix ?? '';
        $delete = "DELETE `" . static::getDbTable() . "`.*\n";
        $from = "FROM `" . $tp . static::getDbTable() . "` AS `" . static::getDbTable() . "`\n";
        $where = 'WHERE ' . static::getDbPrimaryKeyField() . ' = ?';
        $sql = $delete . $from . $where;
        $statement = $this->mysqli->prepare($sql);
        if (!$statement) {
            throw new CoreModelException($this->mysqli->error);
        }
        $statement->bind_param('s', $this->id);
        CoreModel::log($sql, 'debug', [$this->id]);
        if (!$statement->execute()) {
            throw new CoreModelException($statement->error);
        }
        $statement->close();
        CoreModel::$queryCount++;
        $this->reset();
        return $this->app->after($this, __FUNCTION__, null);
    }

    /**
     * Load EAV
     *
     * @return self
     * @throws CoreModelException
     */
    public function loadEav(): CoreModelInterface
    {
        $this->app->before($this, __FUNCTION__, []);
        $eavSources = $this->getEavSources();
        foreach($eavSources as $source) {
            /** @var CoreModelCollectionInterface $eavValueObjects */
            $eavValueObjects = $this->getEavValueObjects($source);
            $eavValues = $this->getValuesPlaceholder($source);
            /** @var null|stdClass $eavValueObjects */
            if($eavValueObjects) {
                foreach($eavValueObjects as $valueObject) {
                    if(!isset($this->rawData->{$source}) || !property_exists($this->rawData->{$source}, $valueObject->getConfig()->getSourceField())) {
                        $eavValues->{$valueObject->getConfig()->getSourceField()}
                            = $valueObject->getData($valueObject->getConfig()->getEavValueField());
                    }
                }
            }
            $this->rawData->{$source} = $eavValues;
        }
        return $this->app->after($this, __FUNCTION__, $this);
    }

    /**
     * Get the value of a property as a string
     *
     * @param $property
     * @return string|null
     * @throws CoreModelException
     */
    public function getStringValue($property): ?string
    {
        $mapping = $this->getDataMapping();

        if(!isset($mapping[$property])) {
            // The mapping could not be found in the core mapping. It's time to load the eav mapping
            $mapping = $this->getEavMapping();
        }
        if(isset($mapping[$property])) {
            $field = $mapping[$property];
            $coreSource = static::getDbTable();
            $eavSources = $this->getEavSources();
            $source = array_keys($field)[0];
            $field = reset($field);

            if(!key_exists($field, $this->fields[$source])) {
                throw new CoreModelException("Trying to map non-configured attribute {$field} of " . get_class($this));
            }
            if(!property_exists($this->rawData->{$source}, $field)) {
                if($source == $coreSource) {
                    $this->load();
                }
                else if(in_array($source, $eavSources)) {
                    $this->loadEav();
                }
            }
            if(property_exists($this->rawData->{$source}, $field)) {
                return $this->rawData->{$source}->$field;
            }
            else {
                return null;
            }
        }
        return null;
    }

    /**
     * This method takes a set of property => value pairs and attempts to save these values directly to the db source
     *
     * @param array|stdClass $rawValues
     * @return CoreModel
     * @throws CoreModelException
     */
    public function saveRawValues($rawValues): CoreModelInterface
    {
        $mainTable = static::getDbTable();
        $splitValues = $this->splitPropertiesBySource($rawValues);

        if(property_exists($splitValues, $mainTable)) {
            unset($splitValues->$mainTable->{static::getDbPrimaryKeyField()});
        }
        // Save to the main table
        $this->saveToMainTable($splitValues->$mainTable);
        // Update EAV sources
        foreach($splitValues as $source => $rawValues) {
            if($source != $mainTable) {
                foreach($rawValues as $field => $dbValue) {
                    $this->updateRawEavValue($source, $field, $dbValue);
                }
            }
        }
        $this->reset();
        return $this;
    }

    /**
     * Get a list of the EAV data sources
     * @return string[]
     */
    public function getEavSources(): array
    {
        return array_keys(get_object_vars($this->getEavConfigs()));
    }

    /**
     * Returns the config for a db field used by this model as an object
     *
     * @param string $source
     * @param string $field
     * @return stdClass
     */
    public function getFieldConfig(string $source, string $field): stdClass
    {
        if($source != static::getDbTable()) {
            $this->getEavConfigs();
        }
        $fieldConfig = $this->fields[$source][$field];
        return (object)$fieldConfig;
    }

    /**
     * Return a stdClass object with empty properties for each rawData attribute
     *
     * @param string $source
     * @return stdClass
     */
    public function getValuesPlaceholder(string $source): stdClass
    {
        $placeholder = new stdClass();
        if(isset($this->fields[$source]) && is_iterable($this->fields[$source])) {
            foreach($this->fields[$source] as $property => $void) {
                $placeholder->$property = null;
            }
        }
        return $placeholder;
    }

    /**
     * @return string[]
     */
    public function getPropertyNames(): array
    {
        $eavMapping = $this->getEavMapping();
        return array_keys($eavMapping);
    }

    /**
     * @return stdClass
     * @throws CoreModelException
     */
    public function getFlatData(): stdClass
    {
        $properties = $this->getPropertyNames();
        $result = new stdClass();
        foreach($properties as $property) {
            $result->$property = $this->getStringValue($property);
        }
        return $result;
    }

    /**
     * @param string $collection
     * @param array $collectionItems
     * @param bool $replaceExisting
     * @return self
     */
    public function setPreloadCollectionItems(
        string $collection,
        array $collectionItems,
        bool $replaceExisting = false
    ): CoreModelInterface
    {
        if(!isset($this->collections->{$collection}) || $replaceExisting) {
            $this->collections->{$collection} = $collectionItems;
        }
        return $this;
    }

    /**
     * Check if a one-to-many collection belonging to the model has loaded
     *
     * @param string $reference
     * @return bool
     */
    public function hasLoadedCollection(string $reference): bool {
        return isset($this->collections->{$reference});
    }

    /**
     * Return a collection of other models based on its one-2-many relation
     *
     * @param string $reference
     * @return CoreModelCollectionInterface|null
     * @throws CoreModelException
     */
    public function getCollection(string $reference): ?CoreModelCollectionInterface
    {
        /**
         * @var array{
         *     model: class-string<CoreModelInterface>,
         *     foreignTable: string,
         *     foreignKey: string,
         *     filters: array,
         *     callback: callable|null
         * }|null $one2Many
         */
        $one2Many = static::hasMany($reference);
        /** @var class-string<CoreModelInterface>|null $linkedModel */
        $linkedModel = $one2Many['model'] ?? null;
        /** @var string $foreignTable */
        $foreignTable = $one2Many['foreignTable'] ?? '';
        /** @var string $foreignKey */
        $foreignKey = $one2Many['foreignKey'] ?? '';
        if(!$linkedModel) {
            return null;
        }
        $linkedCollection = $this->app->getModelCollection($linkedModel);
        $linkedCollection->setFilters($one2Many['filters'] ?? []);
        $id = $this->getId();
        if (isset($id)) {
            $linkedCollection->addFilter(['`' . $foreignTable . '`.`' . $foreignKey . '`' => $id]);
        }
        else {
            $linkedCollection->addFilter('FALSE', true);
        }
        $linkedCollection->lockFilters();
        $linkedCollection->addSortOrder($linkedModel::getDbPrimaryKeyField());
        if(isset($one2Many['callback']) && is_callable($one2Many['callback'])){
            $one2Many['callback']($linkedCollection);
        }
        if(!isset($this->collections->{$reference})) {
            $this->collections->{$reference} = $linkedCollection->getItemsArray();
        }
        $linkedCollection->loadFromArray($this->collections->{$reference});
        /** Clone the collection to avoid problems when iterating over nested loops */
        return clone $linkedCollection;
    }
}