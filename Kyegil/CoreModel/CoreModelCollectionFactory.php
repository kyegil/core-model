<?php

namespace Kyegil\CoreModel;


use Kyegil\CoreModel\Interfaces\CoreModelCollectionInterface;
use Kyegil\CoreModel\Interfaces\CoreModelInterface;

/**
 * Class CoreModelFactory
 * @package Kyegil\CoreModel
 */
class CoreModelCollectionFactory
{
    /**
     * @param $model
     * @param array $di Dependencies
     * @return CoreModelCollection
     * @throws CoreModelException
     */
    public static function create($model, $di) {
        if(is_array($model)) {
            return new MixedCollection($model, $di);
        }
        if(!is_a($model, CoreModelInterface::class, true)) {
            throw new CoreModelException($model . ' is not a ' . CoreModelInterface::class . ' class.');
        }
        $collectionModel = $model::getCollectionModel();
        if(!is_a($collectionModel, CoreModelCollectionInterface::class, true)) {
            throw new CoreModelException($model . ' does not have a ' . CoreModelCollectionInterface::class . ' collection model assigned.');
        }
        return new $collectionModel($model, $di);
    }
}