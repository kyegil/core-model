<?php

namespace Kyegil\CoreModel;


use Kyegil\CoreModel\Interfaces\CoreModelInterface;
use Kyegil\CoreModel\Interfaces\CoreModelRepositoryInterface;

/**
 * Class CoreModelRepository
 * @package Kyegil\CoreModel
 */
class CoreModelRepository implements CoreModelRepositoryInterface
{
    /**
     * @var array
     */
    protected $repository = [];
    /**
     * @var array
     */
    protected $collections = [];

    /**
     * Save an object to the repository
     *
     * @param CoreModelInterface $object
     * @return CoreModelInterface
     * @throws CoreModelException
     */
    public function save(CoreModelInterface $object) {
        $class = get_class($object);
        $id = strval($object);
        if($id == '') {
            throw new CoreModelException('Cannot save object to repository without id');
        }
        $this->repository[$class][$id] = $object;
        return $this->repository[$class][$id];
    }

    /**
     * Get an object from the repository
     *
     * @param string $class
     * @param int|string $id
     * @return mixed
     * @throws CoreModelException
     */
    public function get(string $class, $id) {
        if(!is_a($class, CoreModelInterface::class, true)) {
            throw new CoreModelException($class . ' is not a Kyegil\\CoreModel\\CoreModel class.');
        }
        if(isset($this->repository[$class][$id])) {
            return $this->repository[$class][$id];
        }
        else {
            return null;
        }
    }

    /**
     * Remove an object from the repository
     *
     * @param string $class
     * @param $id
     * @return $this
     */
    public function remove(string $class, $id)
    {
        unset($this->repository[$class][$id]);
        return $this;
    }

    /**
     * Clear
     *
     * @return $this
     */
    public function clear()
    {
        $this->repository = [];
        return $this;
    }
}