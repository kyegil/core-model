<?php

namespace Kyegil\CoreModel;


use Kyegil\CoreModel\Interfaces\CoreModelInterface;

/**
 * Class CoreModelFactory
 * @package Kyegil\CoreModel
 */
class CoreModelFactory
{
    /**
     * @param string $model
     * @param array $di Dependency Injection
     * @param null|int|string $id
     * @return CoreModelInterface
     * @throws CoreModelException
     */
    public static function create($model, $di, $id = null) {
        if(!is_a($model, CoreModelInterface::class, true)) {
            throw new CoreModelException($model . ' is not a Kyegil\\CoreModel\\CoreModelInterface class.');
        }
        return new $model($di, $id);
    }
}