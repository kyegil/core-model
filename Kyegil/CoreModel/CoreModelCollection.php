<?php /** @noinspection DuplicatedCode */

namespace Kyegil\CoreModel;


use Closure;
use Exception;
use Iterator;
use Kyegil\CoreModel\Interfaces\AppInterface;
use Kyegil\CoreModel\Interfaces\CoreModelCollectionInterface;
use Kyegil\CoreModel\Interfaces\CoreModelInterface;
use Kyegil\CoreModel\Interfaces\CoreModelRepositoryInterface;
use mysqli;
use stdClass;

/**
 * Class CoreModelCollection
 * @package Kyegil\CoreModel
 */
class CoreModelCollection implements CoreModelCollectionInterface
{
    const MYSQL_LOGICAL_OPERATORS = ['and', 'or', 'xor', 'not', '&&', '||', '!'];

    /*
     * The current position while iterating over the loaded models
     */
    private int $position = 0;

    /** @var CoreModelRepositoryInterface */
    private $repository;

    /** @var CoreModelFactory */
    private $coreModelFactory;

    /**
     * Object listeners organized by event,
     * as an array of callables
     *
     * @var array<string, callable[]>
     * @see on()
     * @see getListeners()
     * @see clearListeners()
     * @see trigger()
     */
    protected array $listeners = [];

    /** @var null|CoreModelInterface[]|stdClass[] */
    protected ?array $items = null;

    /** @var string[]|int[]|null  */
    protected ?array $itemIds = null;

    /** @var mysqli */
    protected $mysqli;

    /**
     * The main model which this is a collection of
     *
     * @var class-string<CoreModelInterface>
     * @see getModel()
     */
    protected string $model;

    /**
     * The database table which holds the primary key of the model
     *
     * @var string
     */
    protected string $dbTable;

    /**
     * The primary key of the model
     *
     * @var string
     */
    protected string $dbPrimaryKeyField;

    /**
     * The db fields belonging to the main model.
     * These are the default fields to load from the database
     *
     * @var string[]
     */
    protected array $mainModelFields;

    /**
     * Any additional fields or expressions to be queried from the database
     * alongside the main model's fields
     *
     * These are indexed by their db field alias,
     * and described in the form of an object with the properties:
     * * table string|null The table which holds the requested field. Leave blank for expressions.
     * * field string|null The requested field. Leave blank for expressions.
     * * expression string|null A valid query expression, e.g. 'CONCAT(`a`, `b`)'. Leave blank when requesting a db field.
     * * aggregate string|null A valid aggregate funcion which, if provided, will enclose the field or expression, e.g. 'SUM'.
     * * property string The model property which the requested value will be assigned to, e.g. same as the field name.
     * * namespace string The namespace of the model property, e.g. the table name.
     * The namespace and the proerty will be combined as `namespace.property`,
     * and can be used to assign the value to any of the namespaced models in the query.
     *
     * @var array<string, object{table: string, field: string, expression: ?string, aggregate: ?string, property: ?string, namespace: ?string}>
     *
     * @see addField()
     * @see addExpression()
     * @see addModel()
     * @see getAdditionalFields()
     * @see stringifyAdditionalFieldsForQuery()
     * @see clearAdditionalFields()
     *
     */
    protected array $additionalFields = [];

    /**
     * @var array<string, string>
     *
     * @see setJoins()
     * @see addInnerJoin()
     * @see addLeftJoin()
     * @see addSubQueryJoin()
     * @see getJoins()
     * @see removeJoin()
     * @see getBindValuesForJoins()
     */
    protected array $joins = [];

    /**
     * When filtering a query using values from a joined table,
     * the requested value will be stored in this property
     * in order of appearance
     *
     * @var array<string, string[]>
     *     The bind values for prepared statements that follow the joins.
     *     Connected to each join statement by key
     * @see getBindValuesForJoins()
     */
    protected array $bindValuesForJoins = [];

    /**
     * WHERE-filters
     *
     * This array contains all the WHERE-conditions of the query,
     * either in the form of a string,
     * or as an array where the left and right side of the predicate formula
     * are stored as the key and value elements respectively.
     *
     * When using the array format, the right side of the formula
     * (stored as the element value)
     * will be escaped to secure against SQL injection attacks
     *
     * The key element of the array may contain a comparison operator at the very end,
     * preceded by a space character.
     * If no comparison operator is used, then an equation (=) is assumed
     *
     * Examples:
     * * 'one_value >= another_value'
     * * 'date < NOW()'
     * * ['one_value >' => 10]
     * * ['model' => 'CoreModel']
     *
     * Allowed comparison operators:
     * =, >=, >, <=, <>, IS, NOT, LIKE, IN
     *
     * @var string[]|array[]
     * @see setFilters()
     * @see addFilter()
     * @see filterByItemIds()
     * @see getFilters()
     * @see resetFilters()
     * @see lockFilters()
     */
    protected array $filters = [];
    
    /**
     * @var string[]|array[]
     * @see setHaving()
     * @see addHaving()
     * @see getHaving()
     */
    protected array $having = [];

    /**
     * Locked filters
     *
     * Locked filters cannot be reset
     *
     * As a precaution, always lock filters after all have been applied,
     * to avoid any accidental removal later on.
     *
     * @var string[]|array[]
     * @see lockFilters()
     */
    protected array $lockedFilters = [];

    /**
     * The fields used in the GROUP BY statement
     *
     * @var string[]
     * @see setGroupFields()
     * @see addGroupField()
     * @see getGroupFields()
     */
    protected array $groupFields = [];

    /**
     * SORT ORDER fields and direction

     * @var string[]
     * @see addSortOrder()
     * @see getSortOrder()
     * @see resetSortOrder()
     */
    protected array $sortOrder = [];

    /**
     * @var int
     * @see setStart()
     * @see getStart()
     * @see setChunk()
     */
    protected int $start = 0;

    /**
     * @var int|null
     * @see setLimit()
     * @see getLimit()
     */
    protected ?int $limit = null;

    /**
     * @var int|null
     */
    protected ?int $totalRows = null;

    /**
     * @var int|null
     */
    protected ?int $chunkRows = null;

    /** @var AppInterface */
    protected $app;

    /**
     * @var bool True if the collection has been amended since last load
     */
    protected bool $isAmended = false;

    /**
     * @var string|null The last executed query
     * @see getQuery()
     */
    protected ?string $query = null;

    /**
     * @var array|object[] Dependency Injections prepared for all the items models
     */
    protected array $modelDiConfigs = [];

    /**
     * Any separate collections following this collection.
     *
     * Followers are collections that depend on the results from this collection,
     * i.e. one-to-many relations with a foreign key linked to this collection
     *
     * Followers will be loaded together with this collection
     *
     * The followers are referred to by a reference name.
     * Each reference contain an object with the following properties:
     * * collection: The followers collection object
     * * groupedItems: The model items loaded in the follower, organized according to
     * their relation to the item ids in this collection:
     *   * [<item-Id this collection> => $item[]]
     *
     * @var array<string, object{collection: CoreModelCollectionInterface, groupedItems: array<int|string, object[]|CoreModelInterface[]>}>
     * @see linkCollection()
     */
    protected array $followers = [];

    /**
     * If this collection is following another collection,
     * Then this property will hold and object with:
     * * the reference identifying the configuration of the models hasMany relation.
     * * the configuration of the models hasMany relation.
     * * the collection which is being followed.
     *
     * @see CoreModel::hasMany()
     *
     * @var object{reference: string, configuration: array, collection: CoreModelCollectionInterface}|null
     */
    protected ?object $following = null;

    /**
     * @param stdClass $values
     * @param array $bindValues
     * @return string
     */
    private function createSetSyntax(stdClass $values, array &$bindValues): string
    {
        $fields = [];
        foreach ($values as $key => $value) {
            if (is_null($value)) {
                $fields[] = "$key = NULL\n";
            } else {
                $fields[] = "$key = ?\n";
                $bindValues[] = $value;
            }
        }
        return "SET\n" . implode(', ', $fields);
    }

    /**
     * Throw Exception if filters are not allowed
     * @return void
     * @throws SubcollectionFilterException
     */
    private function checkReducability(): void
    {
        if (!$this->isReducable()) {
            throw new SubcollectionFilterException('The collection cannot be reduced.');
        }
    }

    /**
     * Update db values straight to the model's table using the current filters
     *
     * It is strongly adviced to lock your filters before performing mass updates
     *
     * @param stdClass $values
     * @return CoreModelCollection
     * @throws CoreModelException
     * @see CoreModelCollection::lockFilters()
     *
     */
    protected function saveToMainTable(stdClass $values): CoreModelCollectionInterface
    {
        $tp = CoreModel::$tablePrefix;
        $bindValues = [];
        $update = "UPDATE `{$tp}{$this->dbTable}` AS `{$this->dbTable}`\n" . implode("\n", $this->getJoins()) . "\n";
        $set = $this->createSetSyntax($values, $bindValues);
        if (!$this->getFilters()) {
            throw new CoreModelException('Cannot update unfiltered table content');
        }
        $where = "WHERE\n" . $this->prepareFiltersQuery('and', $this->getFilters(), $bindValues) . "\n";
        $sortOrder = $this->getSortOrder() ? "ORDER BY\n" . implode(',', $this->getSortOrder()) . "\n" : '';
        $limit = $this->getLimit() !== null ? "LIMIT\n" . "{$this->getStart()}, {$this->getLimit()}" . "\n" : '';
        $sql = $update . $set . $where . $sortOrder . $limit;
        $statement = $this->mysqli->prepare($sql);
        if (!$statement) {
            throw new CoreModelException($this->mysqli->error);
        }

        if($bindValues) {
            $types = str_repeat('s', count($bindValues));
            $statement->bind_param($types, ...$bindValues);
        }
        CoreModel::log($sql, 'debug', $bindValues);
        if (!$statement->execute()) {
            throw new CoreModelException($statement->error);
        }
        $statement->close();
        CoreModel::$queryCount++;
        /** @var CoreModel $item */
        foreach($this->getItemsArray() as $item) {
            $item->reset();
        }
        $this->unload();
        return $this;
    }

    /**
     * Update db values straight to the model's for the currently loaded items
     *
     * @param stdClass $values
     * @return CoreModelCollection
     * @throws CoreModelException
     */
    protected function saveToMainTableOnItemIds(stdClass $values): CoreModelCollectionInterface
    {
        $itemIds = $this->getItemIds();
        $tp = CoreModel::$tablePrefix;
        $bindValues = [];
        $update = "UPDATE\n{$tp}{$this->dbTable} AS {$this->dbTable}\n" . implode("\n", $this->getJoins()) . "\n";
        $set = $this->createSetSyntax($values, $bindValues);
        $where = "WHERE\n" . $this->prepareFiltersQuery('and', ["{$this->dbTable}.{$this->dbPrimaryKeyField} IN" => $itemIds], $bindValues) . "\n";
        $sql = $update . $set . $where;
        $statement = $this->mysqli->prepare($sql);
        if (!$statement) {
            throw new CoreModelException($this->mysqli->error);
        }

        $types = str_repeat('s', count($bindValues));
        $statement->bind_param($types, ...$bindValues);
        CoreModel::log($sql, 'debug', $bindValues);
        if (!$statement->execute()) {
            throw new CoreModelException($statement->error);
        }
        $statement->close();
        CoreModel::$queryCount++;
        /** @var CoreModel $item */
        foreach($this->getItemsArray() as $item) {
            $item->reset();
        }
        $this->unload();
        return $this;
    }

    /**
     * Prepare predicate
     *
     * Prepares a Sql statement
     *
     * @param string $expression left part of the statement
     * @param mixed|null $value value that may need escaping
     * @return string Sql predicate
     */
    protected function preparePredicate(string $expression = '', $value = null): string
    {
        if(
            strpos($expression, '?') === false
            && !in_array(mb_substr($expression, -1), ['=', '<', '>'])
            && !in_array(mb_substr($expression, -2), ['<=', '>=', '<>'])
            && strtoupper( mb_substr($expression, -3) ) != ' IS'
            && strtoupper( mb_substr($expression, -4) ) != ' NOT'
            && strtoupper( mb_substr($expression, -5) ) != ' LIKE'
            && strtoupper( mb_substr($expression, -3) ) != ' IN'
        ) {
            switch (gettype($value)) {
                case 'NULL':
                    $expression .= ' IS';
                    break;
                case 'array':
                    $expression .= ' IN';
                    break;
                default:
                    $expression .= ' =';
            }
        }

        if (strtoupper( mb_substr($expression, -3) ) == ' IN') {
            $value = is_array($value) ? $value : [$value];
            if (count($value)) {
                $expression .= '(' . str_repeat('?,', count($value)-1) . '?)';
            }
            else {
                // This subquery should always return an empty set,
                // so that the IN() condition will evaluate to false
                $expression .= '(SELECT NULL FROM dual WHERE FALSE)';
            }
        }
        else if (strpos($expression, '?') === false){
            $expression .= is_null($value) ? ' NULL' : ' ?';
        }
        return $expression;
    }

    /**
     * Load an included collection
     * (i.e. a collection that has a one-2-many relation with this collection)
     *
     * @param string $reference
     * @return CoreModelCollection
     * @throws CoreModelException
     */
    protected function loadIncludedCollection(string $reference): CoreModelCollectionInterface
    {
        /** @var object|null $inclusion */
        $inclusion = $this->followers[$reference] ?? null;

        if ($inclusion) {
            /** @var array $config */
            $config = $this->getModel()::hasMany($reference);

            /** @var CoreModelCollectionInterface $collection */
            $collection = $inclusion->collection;
            settype($inclusion->groupedItems, 'array');

            foreach ($collection as $foreignModel) {
                $localId = $foreignModel->getRawData()->{$config['foreignTable']}->{$config['foreignKey']};
                settype($inclusion->groupedItems[$localId], 'array');
                $inclusion->groupedItems[$localId][] = $foreignModel;
            }

            foreach($this->getFormattedItems() as $item) {
                $item->setPreloadCollectionItems($reference, $inclusion->groupedItems[$item->getId()] ?? []);
            }
        }
        return $this;
    }

    /**
     * Load all the included collections
     * (i.e. collections that have a one-2-many relation with this collection)
     *
     * @return CoreModelCollection
     * @throws CoreModelException
     */
    protected function loadIncludedCollections(): CoreModelCollectionInterface
    {
        /**
         * @var string $reference
         * @var object $inclusion
         */
        foreach(array_keys($this->followers) as $reference) {
            $this->loadIncludedCollection($reference);
        }
        return $this;
    }

    /**
     * CoreModelCollection constructor.
     *
     * @param class-string<CoreModelInterface> $model
     * @param object[] $di Dependencies
     * @throws CoreModelDiException
     * @throws CoreModelException
     */
    public function __construct(
        string $model,
        array $di
    )
    {
        if(!is_a($model, CoreModelInterface::class, true)) {
            throw new CoreModelException($model . ' is not a Kyegil\\CoreModel\\CoreModelInterface class.');
        }

        $this->modelDiConfigs = $di;

        $diRequired = [
            AppInterface::class,
            mysqli::class,
            CoreModelRepositoryInterface::class,
            CoreModelFactory::class,
        ];
        foreach($diRequired as $class) {
            if(!isset($di[$class]) || !is_a($di[$class], $class)) {
                throw new CoreModelDiException("Required instance of " . $class . " has not been injected to " . static::class . " constructor");
            }
        }

        $this->position = 0;
        $this->model = $model;
        $this->app = $di[AppInterface::class];
        $this->mysqli = $di[mysqli::class];
        $this->repository = $di[CoreModelRepositoryInterface::class];
        $this->coreModelFactory = $di[CoreModelFactory::class];
        $this->dbTable = $this->model::getDbTable();
        $this->dbPrimaryKeyField = $this->model::getDbPrimaryKeyField();
        $this->mainModelFields = array_keys($this->model::getDbFields());
        $this->groupFields = ["`{$this->dbTable}`.`{$this->dbPrimaryKeyField}`"];
    }

    /**
     * Remove linked collections when cloned
     * @return void
     */
    public function __clone()
    {
        $this->followers = [];
        $this->listeners = [];
    }

    /**
     * Return the current element in a foreach loop
     *
     *  Required by the \Iterator interface
     *
     * @return CoreModel|null
     * @throws CoreModelException
     * @see Iterator::current
     * @link https://www.php.net/manual/en/iterator.current.php
     */
    public function current(): ?CoreModelInterface
    {
        return $this->getCurrentItem();
    }

    /**
     * Return the key of the current element
     *
     * Required by the \Iterator interface
     *
     * @return int
     * @link https://www.php.net/manual/en/iterator.key.php
     * @see Iterator::key
     */
    public function key(): int
    {
        return $this->position;
    }

    /**
     * Move forward to next element
     *
     * Required by the \Iterator interface
     *
     * @link https://www.php.net/manual/en/iterator.next.php
     * @see Iterator::next
     */
    public function next()
    {
        ++$this->position;
    }

    /**
     * Rewind the Iterator to the first element
     *
     * Required by the \Iterator interface
     *
     * @link https://www.php.net/manual/en/iterator.rewind.php
     * @see Iterator::rewind
     */
    public function rewind()
    {
        $this->position = 0;
    }

    /**
     * Checks if current position is valid
     * Required by the \Iterator interface
     *
     * @link https://www.php.net/manual/en/iterator.valid.php
     * @return bool
     * @throws CoreModelException
     */
    public function valid(): bool
    {
        $this->load();
        return isset($this->items[$this->position]);
    }

    /**
     * @param string $event
     * @param callable $callable
     * @return self
     * @see self::$listeners
     */
    public function on(string $event, callable $callable): CoreModelCollectionInterface
    {
        settype($this->listeners[$event], 'array');
        $this->listeners[$event][] = $callable;
        return $this;
    }

    /**
     * @param string $event
     * @return callable[]
     * @see self::$listeners
     */
    public function getListeners(string $event): array
    {
        return $this->listeners[$event] ?? [];
    }

    /**
     * @param string $event
     * @return self
     * @see self::$listeners
     */
    public function clearListeners(string $event): CoreModelCollectionInterface
    {
        $this->listeners[$event] = [];
        return $this;
    }

    /**
     * @param string $event
     * @param array $var
     * @return self
     */
    protected function trigger(string $event, array $var = []): CoreModelCollectionInterface
    {
        foreach ($this->getListeners($event) as $callable) {
            $callable($var);
        }
        return $this;
    }

    /**
     * Set the main model fields
     *
     * If not all the main model fields are required,
     * the ones needed can be set here for a more efficient query.
     *
     * Passing null will reset to include all fields of the model.
     * The primary key will always be included
     *
     * Passing null will reset to all
     *
     * @param string[]|null $fields
     * @return self
     */
    public function setMainModelFields(?array $fields = null): CoreModelCollectionInterface
    {
        $fields = $fields ?? array_keys($this->model::getDbFields());
        $fields = array_intersect($fields, array_keys($this->model::getDbFields()));
        if(!in_array($this->model::getDbPrimaryKeyField(), $fields)) {
            array_unshift($fields, $this->model::getDbPrimaryKeyField());
        }
        if($this->following) {
            $foreignKey = $this->following->configuration['foreignKey'];
            if(!in_array($foreignKey, $fields)) {
                array_unshift($fields, $foreignKey);
            }
        }
        $this->mainModelFields = $fields;
        return $this;
    }

    /**
     * Add a field to the db query, in addition to the models main fields.
     *
     * Any existing fields with the same field name/alias will be overwritten.
     *
     * Adding a field to the query which has already loaded, will not force a reload.
     * In order to force the query to load with the new fields, the existing data must first be unloaded
     * using the `CoreModelCollection::unload()` method
     *
     * @param string $table
     * @param string $field
     * @param string|null $aggregate
     * @param string|null $property
     * @param string|null $namespace
     * @return self
     * @see self::$additionalFields
     */
    public function addField(
        string $table,
        string $field,
        ?string $aggregate = null,
        ?string $property = null,
        ?string $namespace = null
    ): CoreModelCollectionInterface
    {
        $property = $property ?: $field;
        $namespace = $namespace ?: $table;
        $index = $namespace . '.' . $property;
        $this->additionalFields[$index] = (object)[
            'table' => $table,
            'field' => $field,
            'expression' => null,
            'aggregate' => $aggregate,
            'property' => $property ?: $field,
            'namespace' => $namespace ?: $table
        ];
        return $this;
    }

    /**
     * Add a formula expression to the db query
     *
     * This method will not force a reload of existing data.
     * In order to force the query to load with the new fields, the existing data must first be unloaded
     * using the `CoreModelCollection::unload()` method
     *
     * @param string $namespace
     * @param string $property
     * @param string $expression
     * @param string|null $aggregate
     * @return self
     * @see self::$additionalFields
     */
    public function addExpression(
        string $namespace,
        string $property,
        string $expression,
        ?string $aggregate = null
    ): CoreModelCollectionInterface
    {
        $index = $namespace . '.' . $property;
        $this->additionalFields[$index] = (object)[
            'table' => null,
            'field' => null,
            'expression' => $expression,
            'aggregate' => $aggregate,
            'property' => $property,
            'namespace' => $namespace
        ];
        return $this;
    }

    /**
     * Add all the fields of an external model to the collection.
     *
     * The table joins must be added separately
     *
     * @param string $model
     * @param string|null $namespace
     * @param string|null $source
     * @return self
     * @throws CoreModelException
     * @see self::$additionalFields
     */
    public function addModel(string $model, ?string $namespace = null, ?string $source = null): CoreModelCollectionInterface
    {
        if (!is_a($model, CoreModelInterface::class, true)) {
            throw new CoreModelException( $model . ' is not a ' . CoreModelInterface::class);
        }
        $table = $source ?? $model::getDbTable();
        $fields = array_keys($model::getDbFields());
        foreach($fields as $field) {
            $this->addField($table, $field, null, null, $namespace);
        }
        return $this;
    }

    /**
     * List all the fields that have been reguested in addition to the ones provided by the model
     *
     * @return stdClass[]
     * @see self::$additionalFields
     */
    public function getAdditionalFields(): array
    {
        return $this->additionalFields;
    }

    /**
     * Format all additional fields into valid SQL syntax using namespaces
     *
     * Examples:
     *
     * * field: 'field'
     * * table: 'table'
     * * property: 'property'
     * * namespace: 'namespace'
     * ```
     * `table`.`field` AS `namespace.property`
     * ```
     *
     * * field: 'field'
     * * table: 'table'
     * * property: 'property'
     * * namespace: 'custom.namespace'
     * * aggregate: 'SUM'
     * ```
     * SUM(`table`.`field`) AS `custom.namespace.property`
     * ```
     *
     * * expression: '`price` * `quantity_in_stock`'
     * * property: 'value'
     * * namespace: 'products'
     * ```
     * `price` * `quantity_in_stock` AS `products.value`
     * ```
     *
     * * expression: '`price` * `quantity_in_stock`'
     * * property: 'average_value'
     * * namespace: 'inventories'
     * * aggregate: 'AVG'
     * ```
     * AVG(`price` * `quantity_in_stock`) AS `inventories.average_value`
     * ```
     *
     * @return string[]
     * @see self::$additionalFields
     */
    public function stringifyAdditionalFieldsForQuery(): array
    {
        $result = [];
        foreach($this->getAdditionalFields() as $field) {
            if (isset($field->aggregate)) {
                $result[] = $field->expression
                    ? "{$field->aggregate}({$field->expression}) AS `{$field->namespace}.{$field->property}`"
                    : "{$field->aggregate}(`{$field->table}`.`{$field->field}`) AS `{$field->namespace}.{$field->property}`"
                ;
            }
            else {
                $result[] = $field->expression
                    ? "{$field->expression} AS `{$field->namespace}.{$field->property}`"
                    : "`{$field->table}`.`{$field->field}` AS `{$field->namespace}.{$field->property}`"
                ;
            }
        }
        return $result;
    }

    /**
     * Clear all the additional fields
     *
     * Removes all fields except from the main model from the query.
     *
     * @return self
     * @see self::$additionalFields
     */
    public function clearAdditionalFields(): CoreModelCollectionInterface
    {
        $this->unload();
        $this->additionalFields = [];
        return $this;
    }

    /**
     * Replace all the JOIN statements in one go
     *
     * The table alias should be used as a unique reference (key) for each join
     * *
     * @param array<string, string> $joins
     * @return self
     * @see self::$joins
     */
    public function setJoins(array $joins): CoreModelCollectionInterface
    {
        /*
         * If any of the joins is an inner join then this will affect the result set,
         * so we will have to unload the existing set
         */
        if(array_filter($joins, function ($join) {return strpos(trim($join), 'INNER JOIN') === 0; })) {
            $this->unload();
        }
        $this->joins = $joins;
        return $this;
    }

    /**
     * Add an INNER JOIN statement
     *
     * @param string|array|stdClass $table Either passed as table name, or as alias => table name
     * @param string $condition
     * @return self
     * @throws CoreModelException
     * @see self::$joins
     */
    public function addInnerJoin($table, string $condition): CoreModelCollectionInterface
    {
        /*
         * An inner join will affect the result set,
         * so we will have to unload the existing set
         */
        $this->unload();
        settype($table, 'array');
        $tp = CoreModel::$tablePrefix;
        $key = str_replace('`', '', array_keys($table)[0]);
        $table = str_replace('`', '', reset($table));
        $alias = is_numeric($key) ? $table : $key;
        $join = "INNER JOIN `{$tp}{$table}` AS `{$alias}` ON {$condition}";
        // Throw exception if alias is already in use
        if (isset($this->joins[$alias]) && str_replace('`', '', $this->joins[$alias]) != str_replace('`', '', $join)) {
            throw new CoreModelException("Cannot redeclare table alias `{$alias}`");
        }
        $this->joins[$alias] = $join;
        return $this;
    }

    /**
     * Add a LEFT JOIN statement
     *
     * @param string|array<string, string> $table Either passed as table name, or as alias => table name
     * @param string $condition
     * @return self
     * @throws CoreModelException
     */
    public function addLeftJoin($table, string $condition): CoreModelCollectionInterface
    {
        settype($table, 'array');
        $tp = CoreModel::$tablePrefix;
        $key = str_replace('`', '', array_keys($table)[0]);
        $table = str_replace('`', '', reset($table));
        $alias = is_numeric($key) ? $table : $key;
        $join = "LEFT JOIN `{$tp}{$table}` AS `{$alias}` ON {$condition}";
        // Throw exception if alias is already in use
        if (isset($this->joins[$alias]) && str_replace('`', '', $this->joins[$alias]) != str_replace('`', '', $join)) {
            throw new CoreModelException("Cannot redeclare table alias `{$alias}`");
        }
        $this->joins[$alias] = $join;
        return $this;
    }

    /**
     * Add a JOIN statement containing a subquery
     *
     * @param string $subQuery
     * @param string $alias
     * @param string $joinCondition
     * @param string $joinType LEFT|INNER
     * @param array $bindValues Potential values to add to the prepared statement
     * @return self
     * @throws CoreModelException
     * @see self::$joins
     */
    public function addSubQueryJoin(
        string $subQuery,
        string $alias,
        string $joinCondition,
        string $joinType = 'LEFT',
        array $bindValues = []
    ): CoreModelCollectionInterface
    {
        $joinType = in_array(strtoupper($joinType), ['LEFT', 'INNER']) ? strtoupper($joinType) : 'LEFT';

        /*
         * An inner join will affect the result set,
         * so we will have to unload the existing set
         */
        if($joinType == 'INNER') {
            $this->unload();
        }
        $join = "{$joinType} JOIN ({$subQuery}) AS `{$alias}` ON {$joinCondition}";
        // Throw exception if alias is already in use
        if (isset($this->joins[$alias]) && str_replace('`', '', $this->joins[$alias]) != str_replace('`', '', $join)) {
            throw new CoreModelException("Cannot redeclare table alias `{$alias}`");
        }
        $this->joins[$alias] = $join;
        $this->bindValuesForJoins[$alias] = $bindValues;
        return $this;
    }

    /**
     * Get all the current JOIN statements
     *
     * The table alias is used as the array key for each join
     *
     * @return array<string, string>
     * @see self::$joins
     */
    public function getJoins(): array
    {
        return $this->joins;
    }

    /**
     * Get all the current INNER JOIN statements
     *
     * The table alias is used as the array key for each join
     *
     * @return array<string, string>
     * @see self::$joins
     */
    public function getInnerJoins(): array
    {
        return array_filter($this->joins, function ($join) {
            return strpos($join, 'INNER JOIN') !== false;
        });
    }

    /**
     * Remove an existing JOIN statement by its alias
     *
     * @param string $alias
     * @return self
     * @see self::$joins
     */
    public function removeJoin(string $alias): CoreModelCollectionInterface
    {
        /*
         * An inner join will affect the result set,
         * so we will have to unload the existing set
         */
        if(strpos(trim($this->joins[$alias] ?? ''), 'INNER JOIN') === 0) {
            $this->unload();
        }
        unset($this->joins[$alias]);
        return $this;
    }

    /**
     * Get the bind values used by joins
     *
     * When filtering a query using values from a joined table,
     * the requested value will be stored in this
     *
     * @param string|null $alias
     * @return array
     * @see self::$joins
     * @see self::$bindValuesForJoins
     */
    public function getBindValuesForJoins(?string $alias = null): array
    {
        if($alias) {
            return $this->bindValuesForJoins[$alias] ?? [];
        }
        $values = [];
        foreach(array_keys($this->getJoins()) as $alias) {
            $values = array_merge($values, $this->bindValuesForJoins[$alias] ?? []);
        }
        return $values;
    }

    /**
     * Is this collection reducable?
     *
     * A collection cannot be reduced (filtered or limited)
     * if it is a linked subcollection for another collection
     *
     * @return bool
     */
    public function isReducable(): bool
    {
        return !$this->getCollectionThisIsSubCollectionSourceFor();
    }

    /**
     * Set WHERE filters to the collection.
     * The filters will NOT replace any existing locked filters
     *
     * @param array $filters
     * @param boolean $doNotMold Set true to apply raw filters
     * @return self
     * @throws Exception
     * @see self::$filters
     */
    public function setFilters(array $filters, $doNotMold = false): CoreModelCollectionInterface
    {
        $this->checkReducability();
        if (!$doNotMold) {
            $filters = $this->moldFilters($filters);
        }
        $this->filters = array_merge($this->lockedFilters, $filters);
        return $this;
    }

    /**
     * Add ONE new WHERE filter to the existing filters
     * The existing collection items are unloaded when a filter is applied.
     *
     * The filter can either be given as a prepared predicate string 'id IS NULL',
     * or in the array format ['expression' => value]
     * in which case the value will be bound to a placeholder
     *
     * @param string|string[]|array<string,mixed> $filter
     * @param boolean $doNotMold Set true to apply raw filters
     * @return self
     * @throws Exception
     * @see self::$filters
     */
    public function addFilter($filter, bool $doNotMold = false): CoreModelCollectionInterface
    {
        $this->checkReducability();
        $this->unload();
        if (!$doNotMold) {
            $filter = $this->moldFilters($filter);
        }
        if($filter) {
            $this->filters[] = $filter;
        }
        return $this;
    }

    /**
     * Limit the collection to certain ids
     *
     * @param array $itemIds
     * @return self
     * @throws Exception
     * @see self::$filters
     */
    public function filterByItemIds(array $itemIds): CoreModelCollectionInterface
    {
        $this->checkReducability();
        if(!$itemIds) {
            $this->addFilter(['FALSE']);
        }
        else {
            $this->addFilter(["`{$this->dbTable}`.`{$this->dbPrimaryKeyField}`" => $itemIds]);
        }
        return $this;
    }

    /**
     * Return all the WHERE filters
     *
     * @return array
     * @see self::$filters
     */
    public function getFilters(): array
    {
        $filters = $this->filters;
        $linkSetup = $this->follows();

        if($linkSetup) {
            $followedCollection = $linkSetup->collection;
            $configuration = $linkSetup->configuration;
            $filters[]
                = ['`' . $configuration['foreignTable'] . '`.`' . $configuration['foreignKey'] . '`'
                    => $followedCollection->getItemIds()];
        }
        return $filters;
    }

    /**
     * Reset all the filters (remove all filters except from locked filters)
     *
     * The collection items unload when filters change.
     *
     * @return self
     * @see self::$filters
     * @see self::$lockedFilters
     */
    public function resetFilters(): CoreModelCollectionInterface
    {
        $this->unload();
        $this->filters = $this->lockedFilters;
        $this->having = [];
        return $this;
    }

    /**
     * Lock the current filters to the collection,
     * so they cannot be removed
     *
     * @return self
     * @see self::$filters
     * @see self::$lockedFilters
     */
    public function lockFilters(): CoreModelCollectionInterface
    {
        $this->lockedFilters = $this->filters;
        return $this;
    }

    /**
     * Mold the filters using backticks if desirable
     *
     * This function tries to add backticks to a filter if none exist already.
     * Using backticks will improve clarity in field references
     *
     * Example:
     * * 'my_field' could be molded as `main_table`.`my_field`
     *
     * @param $filter
     * @return array|string
     */
    public function moldFilters($filter)
    {
        if (is_string($filter)) {
            $filterParts = explode(' ', $filter, 2);
            if (count($filterParts) < 2) {
                return $filter;
            }
            $fieldPart = $filterParts[0];
            if (strpos($fieldPart, '.') !== false) {
                if (strpos($fieldPart, '`') === 0) {
                    // if the filter already contains table names in quotes then do nothing
                    return $filter;
                }
                // if the filter contains unquoted table names then add quotes
                return '`' . implode('`.`', explode('.', $fieldPart, 2)) . '` ' . $filterParts[1];
            }
            // if the filter does not contain table name, then add the default table
            if (in_array($fieldPart, array_keys($this->model::getDbFields()))) {
                return '`' . $this->dbTable . '`.`' . $fieldPart . '` ' . $filterParts[1];
            }
        }
        if (is_array($filter)) {
            $moldedFilter = [];
            foreach ($filter as $fieldPart => $expression) {
                $fieldPart = trim($fieldPart);
                if (is_numeric($fieldPart) || in_array(strtolower($fieldPart), self::MYSQL_LOGICAL_OPERATORS)) {
                    $moldedFilter[$fieldPart] = $this->moldFilters($expression);
                }
                else {
                    if (strpos($fieldPart, '.') !== false) {
                        $moldedFilter[$fieldPart] =  $expression;
                    }
                    // if the filter does not contain table name, then add the default table
                    else if (in_array($fieldPart, array_keys($this->model::getDbFields()))) {
                        $moldedFilter[ '`' . $this->dbTable . '`.`' . $fieldPart . '`'] = $expression;
                    }
                    else {
                        // Default to leave the filter unmodified
                        $moldedFilter[$fieldPart] = $expression;
                    }
                }
            }
            return $moldedFilter;
        }
        return $filter;
    }

    /**
     * Prepare Filters Query
     *
     * Preparing a combination of predicates for use in a 'where' or 'having'-condition
     * Each element can be a ready-made string predicate,
     * or an array consisting of expression => value.
     * The expression can be a prepared string using ? as placeholders,
     * or end with a comparison operator:
     * one of '=', '>', '<', '<>', '<=', '>=', 'IS', 'NOT', 'LIKE' or 'IN'
     * If no comparison operator is given, '=' or 'IS' will be added.
     *
     * Expressions can be combined logically
     * using 'and', 'or', 'xor', 'not', '&&', '||' or '!'
     * as the element key
     *
     * @param string $expression
     * @param string|string[]|array[] $value
     * @param array $bindValues
     * @return string
     * @throws CoreModelException
     */
    public function prepareFiltersQuery(
        string $expression,
               $value,
        array &$bindValues
    ): string
    {
        /*
         * String expressions are returned as they are
         */
        if (is_numeric($expression) && is_string($value)) {
            return $value;
        }

        /*
         * Numeric arrays are treated as 'and' combinations
         */
        if (is_numeric($expression) && is_array($value)) {
            return $this->prepareFiltersQuery('and', $value, $bindValues);
        }

        if(is_numeric($expression)) {
            /*
             * Numeric arrays are treated as 'and' combinations
             * unless they only contain a single string
             */
            if (is_string($value)) {
                return $value;
            }
            elseif (is_array($value)) {
                return $this->prepareFiltersQuery('and', $value, $bindValues);
            }
            else {
                throw new CoreModelException('Invalid filter');
            }
        }

        /*
         * Combinations
         */
        if(in_array(trim(strtolower($expression)), ['and', 'or', 'xor', 'not', '&&', '||', '!'])) {
            settype($value, 'array');
            /** @var string[] $predicatesCombination */
            $predicatesCombination = [];
            foreach ($value as $index => $subValue) {
                if (is_array($subValue) && count($subValue) == 1 && is_numeric($index)) {
                    /* We can skip one bracket level */
                    $predicatesCombination[] = $this->prepareFiltersQuery(key($subValue), current($subValue), $bindValues);
                }
                else {
                    $predicatesCombination[] = $this->prepareFiltersQuery($index, $subValue, $bindValues);
                }
            }
            return "(" . implode(")\n" . strtoupper($expression) . " (", $predicatesCombination) . ")\n";
        }

        $predicate = $this->preparePredicate($expression, $value);
        if (is_null($value)) {
            $value = [];
        }
        if (!is_array($value)) {
            $value = [$value];
        }
        if (substr_count($predicate, '?') != count($value)) {
            throw new CoreModelException('Number of values (' . count($value) . ") don't match number of placeholders (" . substr_count($predicate, '?'). "): " . $expression . '. ');
        }
        $bindValues = array_merge($bindValues, array_values($value));
        return $predicate;
    }

    /**
     * Set HAVING filters to the collection.
     * The filters will replace any existing HAVING filters
     *
     * @param array $havingFilters
     * @param boolean $doNotMold Set true to apply raw filters
     * @return self
     * @throws Exception
     * @see self::$having
     */
    public function setHaving(array $havingFilters, bool $doNotMold = false): CoreModelCollectionInterface
    {
        $this->checkReducability();
        if (!$doNotMold) {
            $havingFilters = $this->moldFilters($havingFilters);
        }
        $this->having = $havingFilters;
        return $this;
    }

    /**
     * Add ONE new HAVING filter to the existing filters
     * The collection items unload when a filter is applied.
     *
     * The filter can either be given in the array format ['id' => null] or as a string 'id IS NULL'
     *
     * @param string|array $havingFilter
     * @param boolean $doNotMold Set true to apply raw filters
     * @return self
     * @throws Exception
     * @see self::$having
     */
    public function addHaving($havingFilter, bool $doNotMold = false): CoreModelCollectionInterface
    {
        $this->checkReducability();
        $this->unload();
        if (!$doNotMold) {
            $havingFilter = $this->moldFilters($havingFilter);
        }
        if($havingFilter) {
            $this->having[] = $havingFilter;
        }
        return $this;
    }

    /**
     * Return the HAVING filters
     *
     * @return array
     * @see self::$having
     */
    public function getHaving(): array
    {
        return $this->having;
    }

    /**
     * Set all the GROUP BY fields
     *
     * @param array $groupFields
     * @return self
     * @see self::$groupFields
     */
    public function setGroupFields(array $groupFields): CoreModelCollectionInterface
    {
        $this->unload();
        $this->groupFields = $groupFields;
        return $this;
    }

    /**
     * Add a GROUP BY field
     *
     * @param string $groupField
     * @return self
     * @see self::$groupFields
     */
    public function addGroupField(string $groupField): CoreModelCollectionInterface
    {
        $this->unload();
        $this->groupFields[] = $groupField;
        return $this;
    }

    /**
     * Get the current GROUP BY fields
     *
     * @return string[]
     * @see self::$groupFields
     */
    public function getGroupFields(): array
    {
        return $this->groupFields;
    }

    /**
     * Add a SORT BY field
     *
     * The sort order field must be one of the requested fields.
     *
     * The sort order field should be provided as a simple field name string,
     * without quotes or table notation.
     *
     * If the field does not exist in the main table,
     * then the table name should be provided in the third argument.
     *
     * @param string $field
     * @param bool $descending
     * @param string|null $table
     * @param bool $nullAsMax
     * @return self
     * @see self::$sortOrder
     */
    public function addSortOrder(string $field, bool $descending = false, ?string $table = null, bool $nullAsMax = false): CoreModelCollectionInterface
    {
        $this->unload();
        $direction = $descending ? 'DESC' : 'ASC';
        if(
            !$table
            && strpos($field, '.') === false
            && in_array($field, $this->mainModelFields)
        ) {
            array_unshift($this->sortOrder, "`{$this->dbTable}`.`{$field}` {$direction}");
            if($nullAsMax) {
                array_unshift($this->sortOrder, "`{$this->dbTable}`.`{$field}` IS NULL {$direction}");
            }
            return $this;
        }
        else if($table) {
            foreach ($this->getAdditionalFields() as $additionalField) {
                if ("`{$table}.{$field}`"  == "`{$additionalField->namespace}.{$additionalField->property}`") {
                    array_unshift($this->sortOrder, "`{$table}.{$field}` {$direction}");
                    if($nullAsMax) {
                        array_unshift($this->sortOrder, "`{$table}.{$field}` IS NULL {$direction}");
                    }
                    return $this;
                }
                if ("`{$table}`.`{$field}`"  == "`{$additionalField->namespace}.{$additionalField->field}`") {
                    array_unshift($this->sortOrder, "`{$table}`.`{$field}` {$direction}");
                    if($nullAsMax) {
                        array_unshift($this->sortOrder, "`{$table}.{$field}` IS NULL {$direction}");
                    }
                    return $this;
                }
            }
        }
        return $this;
    }

    /**
     * Returns a list of SORT BY fields with their ASC/DESC direction
     *
     * @return string[]
     * @see self::$sortOrder
     */
    public function getSortOrder(): array
    {
        return $this->sortOrder;
    }

    /**
     * Reset the SORT BY fields
     *
     * @return self
     * @see self::$sortOrder
     */
    public function resetSortOrder(): CoreModelCollectionInterface
    {
        $this->unload();
        $this->sortOrder = [];
        return $this;
    }

    /**
     * Set the start row when retrieving limited chunks (paging)
     *
     * @param int $start
     * @return self
     * @throws Exception
     * @see self::$start
     */
    public function setStart(int $start): CoreModelCollectionInterface
    {
        $this->checkReducability();
        $this->unload();
        $this->start = $start;
        return $this;
    }

    /**
     * Get the current start position
     *
     * @return int
     * @see self::$start
     */
    public function getStart(): int
    {
        return $this->start;
    }

    /**
     * Set the LIMIT
     *
     * @param int|null $limit
     * @return self
     * @throws Exception
     * @see self::$limit
     */
    public function setLimit(?int $limit = null): CoreModelCollectionInterface
    {
        $this->checkReducability();
        $this->unload();
        $this->limit = $limit;
        return $this;
    }

    /**
     * Return the current LIMIT
     *
     * @return int|null
     * @see self::$limit
     */
    public function getLimit(): ?int
    {
        return $this->limit;
    }

    /**
     * Set chunk (page) number for limited collections
     *
     * @param int $chunk
     * @return self
     * @throws Exception
     * @see self::$limit
     * @see self::$start
     */
    public function setChunk(int $chunk): CoreModelCollectionInterface
    {
        $this->checkReducability();
        $this->unload();
        $this->start = (int)$this->limit * $chunk;
        return $this;
    }

    /**
     * Load the collection
     *
     * @param bool $force
     * @return self
     * @throws CoreModelException
     * @see self::$items
     * @see loadFromArray()
     */
    public function load(bool $force = false): CoreModelCollectionInterface
    {
        extract($this->app->before($this, __FUNCTION__, $args = get_defined_vars()), EXTR_IF_EXISTS);
        $tp = CoreModel::$tablePrefix;

        $fields = array_map(function ($field){
            return "`{$this->dbTable}`.`{$field}`";
        }, $this->mainModelFields);
        $fields = array_merge($fields, $this->stringifyAdditionalFieldsForQuery());

        if ($force || !is_array($this->items)) {
            $this->trigger('beforeLoad', [$this]);
            $bindValues = [];
            $select = "SELECT\n" . implode(', ', $fields) . "\n";
            $countSelect = $this->getHaving() ? $select : "SELECT NULL\n";
            $from = "FROM\n`{$tp}{$this->dbTable}` AS `{$this->dbTable}`\n" . implode("\n", $this->getJoins()) . "\n";
            $bindValues = array_merge($bindValues, $this->getBindValuesForJoins());
            $where = $this->getFilters() ? "WHERE\n" . $this->prepareFiltersQuery('and', $this->getFilters(), $bindValues) . "\n" : '';
            $group = $this->getGroupFields() ? "GROUP BY\n" . implode(',', $this->getGroupFields()) . "\n" : '';
            $having = $this->getHaving() ? "HAVING\n" . $this->prepareFiltersQuery('and', $this->getHaving(), $bindValues) . "\n" : '';
            $sortOrder = $this->getSortOrder() ? "ORDER BY\n" . implode(',', $this->getSortOrder()) . "\n" : '';
            $limit = $this->getLimit() !== null ? "LIMIT\n" . "{$this->getStart()}, {$this->getLimit()}" . "\n" : '';
            $sql = $select . $from . $where . $group . $having . $sortOrder . $limit;
            $countSql = $countSelect . $from . $where . $group . $having;
            $countStatement = $this->mysqli->prepare($countSql);
            if (!$countStatement) {
                throw new CoreModelException($this->mysqli->error);
            }
            $statement = $this->mysqli->prepare($sql);
            if (!$statement) {
                throw new CoreModelException($this->mysqli->error);
            }

            $types = str_repeat('s', count($bindValues));
            if($bindValues) {
                $countStatement->bind_param($types, ...$bindValues);
                $statement->bind_param($types, ...$bindValues);
            }
            if ($limit) {
                $countStatement->execute();
                CoreModel::$queryCount++;
                $countResult = $countStatement->get_result();
                if (!$countResult) {
                    throw new CoreModelException($this->mysqli->error);
                }
                $this->totalRows = $countResult->num_rows;
                $countResult->close();
            }
            $countStatement->close();

            CoreModel::log($sql, 'debug', $bindValues);
            $statement->execute();
            CoreModel::$queryCount++;
            $resultSet = $statement->get_result();
            if (!$resultSet) {
                throw new CoreModelException($this->mysqli->error);
            }
            $items = [];
            while ($row = $resultSet->fetch_object()) {
                $items[] = $row;
            }
            $resultSet->close();
            $statement->close();

            $this->query = $sql;
            $this->items = $items;
            $this->isAmended = false;
            $this->chunkRows = count($this->items);
            if (!$limit) {
                $this->totalRows = $this->chunkRows;
            }
            $this->loadIncludedCollections();
            $this->trigger('afterLoad', [$this]);
        }
        return $this->app->after($this, __FUNCTION__, $this);
    }

    /**
     * Populate the collection from an array of existing objects
     *
     * @param CoreModelInterface[] $array
     * @return self
     * @see self::$items
     * @see load()
     */
    public function loadFromArray(array $array): CoreModelCollectionInterface
    {
        $this->items = [];
        $itemIds = [];
        foreach($array as $model) {
            if(is_a($model, $this->getModel(), true)) {
                $this->items[] = $model;
                $itemIds[] = $model->id;
            }
        }

        $this->filters[] = $itemIds
            ? ["`{$this->dbTable}`.`{$this->dbPrimaryKeyField}`" => $itemIds]
            : ['FALSE'];
        $this->lockFilters();
        $this->totalRows = count($this->items);
        $this->chunkRows = count($this->items);
        return $this;
    }

    /**
     * Preload all the Eav attributes for the entire collection
     *
     * @return self
     * @todo Implement collection pre-loading of eav attributes.
     */
    public function loadAllEavAttributes(): CoreModelCollectionInterface {
        // TODO: Implement collection pre-loading of eav attributes.
        return $this;
    }

    /**
     * Unload the collection items, forcing the collection to reload if required
     *
     * @return self
     * @see load()
     */
    public function unload(): CoreModelCollectionInterface
    {
        $this->items = null;
        $this->itemIds = null;
        $this->chunkRows = null;
        $this->totalRows = null;
        foreach($this->followers as $includedCollection) {
            $includedCollection->groupedItems = null;
            $includedCollection->collection->unload();
        }
        return $this;
    }

    /**
     * Check if the collection has loaded
     *
     * @return bool
     * @see load()
     */
    public function hasLoaded(): bool
    {
        return isset($this->items);
    }

    /**
     * Iterate through the collection with a callable
     * Each item will be passed to the callable as the only argument
     *
     * @param callable $action
     * @return self
     * @throws CoreModelException
     */
    public function forEach(callable $action): CoreModelCollectionInterface
    {
        /** @var CoreModelInterface $model */
        foreach($this->getItemsArray() as $model) {
            $action($model);
        }
        return $this;
    }

    /**
     * Convert the collection to an array of models
     *
     * @return CoreModelInterface[]
     * @throws CoreModelException
     */
    public function getItemsArray(): array
    {
        $this->load();
        foreach($this->items as $index => $item) {
            $this->getAt($index);
        }
        return $this->items;
    }

    /**
     * Get the items in this collection that have been formatted as CoreModel instances
     *
     * If the collection has not yet loaded an empty array will be returned
     *
     * @return CoreModel[]
     */
    public function getFormattedItems(): array
    {
        if(empty($this->items)){
            return [];
        }
        return array_filter($this->items, function($item) {
            return $item instanceof CoreModelInterface;
        });
    }

    /**
     * Get the items in this collection that have not yet been formatted as CoreModel instances
     *
     * If the collection has not yet loaded an empty array will be returned
     *
     * @return object[]
     */
    public function getUnformattedItems(): array
    {
        if(empty($this->items)){
            return [];
        }
        return array_filter($this->items, function($item) {
            return !($item instanceof CoreModelInterface);
        });
    }

    /**
     * Return all the id values in the collection
     *
     * @return string[]|int[]
     * @throws CoreModelException
     */
    public function getItemIds(): array
    {
        if(!isset($this->itemIds)) {
            $this->itemIds = [];
            $this->load();
            $int = isset($this->model::getDbFields()[$this->dbPrimaryKeyField]['type']) && in_array($this->model::getDbFields()[$this->dbPrimaryKeyField]['type'], ['int','integer']);
            foreach($this->items as $item) {
                if ($item instanceof CoreModelInterface) {
                    $rawData = $item->getRawData();
                    $id = $rawData->{$this->dbTable}->{$this->dbPrimaryKeyField};
                }
                else {
                    $id = $item->{$this->dbPrimaryKeyField};
                }
                $this->itemIds[] = $int ? (int)$id : $id;
            }
        }
        return $this->itemIds;
    }

    /**
     * Converts a stdClass raw db data object to a CoreModel instance
     *
     * @param object|CoreModelInterface $item
     * @return CoreModelInterface
     * @throws CoreModelException
     */
    public function itemToModel($item): CoreModelInterface
    {
        if($item instanceof CoreModelInterface) {
            return $item;
        }
        if (!is_a($item, stdClass::class, true)) {
            throw new CoreModelException('Unrecognised item, cannot convert to Model');
        }
        $itemId = $item->{$this->dbPrimaryKeyField};
        /** @var CoreModel|null $model */
        $model = $this->repository->get($this->model, $itemId);
        if (!$model) {
            // Prepare dependency injection
            $di = [
                AppInterface::class => $this->app,
                mysqli::class => $this->mysqli,
                CoreModelRepositoryInterface::class => $this->repository,
            ];
            /** @var CoreModel $model */
            $model = $this->coreModelFactory::create($this->model, $di, $itemId);
        }
        if (!$model->getForceReload()) {
            $model->setPreloadedValues($model->mapNamespacedRawData($item));
            foreach($this->followers as $reference => $inclusion) {
                $model->setPreloadCollectionItems($reference, $inclusion->groupedItems[$itemId] ?? []);
            }
        }
        $this->repository->save($model);

        return $model;
    }

    /**
     * Convert all data items to CoreModel instances
     *
     * @return self
     * @throws CoreModelException
     */
    public function formatAllItems(): CoreModelCollectionInterface
    {
        if( !$this->hasLoaded()) {
            $this->load();
        }
        foreach ($this->items as &$item) {
            $item = $this->itemToModel($item);
        }
        return $this;
    }

    /**
     * Get the model at the specified index
     *
     * @param int $index
     * @return CoreModel|null
     * @throws CoreModelException
     */
    public function getAt(int $index): ?CoreModelInterface
    {
        if (!$this->hasLoaded()) {
            $this->load();
        }
        $item = $this->items[$index] ?? null;
        if( !$item || $item instanceof CoreModelInterface) {
            return $item;
        }
        $this->items[$index] = $this->itemToModel($item);
        return $this->items[$index];
    }

    /**
     * Get the current item
     *
     * @return CoreModel|null
     * @throws CoreModelException
     */
    public function getCurrentItem(): ?CoreModelInterface
    {
        return $this->getAt($this->position);
    }

    /**
     * Return the first item in the collection
     *
     * @return CoreModel|null
     * @throws CoreModelException
     */
    public function getFirstItem(): ?CoreModelInterface
    {
        $firstItem = null;
        $this->load();
        if(count($this->items)) {
            $firstItem = $this->getAt(0);
        }
        return $firstItem;
    }

    /**
     * Return a random item from the collection
     *
     * @return CoreModel|null
     * @throws CoreModelException
     */
    public function getRandomItem(): ?CoreModelInterface
    {
        $randomItem = null;
        $this->load();
        if(count($this->items)) {
            $randomIndex = rand(0, count($this->items) - 1);
            $randomItem = $this->getAt($randomIndex);
        }
        return $randomItem;
    }

    /**
     * Return the last item in the collection
     *
     * @return CoreModel|null
     * @throws CoreModelException
     */
    public function getLastItem(): ?CoreModelInterface
    {
        $lastItem = null;
        $this->load();
        if(count($this->items)) {
            end($this->items);
            $endIndex = key($this->items);
            $lastItem = $this->getAt($endIndex);
        }
        return $lastItem;
    }

    /**
     * Get the size of the current chunk of data if using LIMIT
     *
     * @return int
     * @throws CoreModelException
     */
    public function getChunkRows(): int
    {
        if(!isset($this->chunkRows)) {
            $this->load();
        }
        return $this->chunkRows;
    }

    /**
     * Get the total row count ignoring any LIMIT
     *
     * @return int
     * @throws CoreModelException
     */
    public function getTotalRows(): int
    {
        if(!isset($this->totalRows)) {
            $this->load();
        }
        return $this->totalRows;
    }

    /**
     * Mix/merge this collection with another
     *
     * @param CoreModelCollectionInterface $collection
     * @return MixedCollection
     */
    public function mixWith(CoreModelCollectionInterface $collection): CoreModelCollectionInterface {
        return $this->app->getModelCollection([$this, $collection]);
    }

    /**
     * Remove a single item from the collection
     *
     * WARNING: Amending a collection this way will make it unreliable for reloading
     *
     * @param CoreModelInterface $itemToRemove
     * @return self
     * @throws CoreModelException
     * @see isAmended()
     */
    public function removeItem(CoreModelInterface $itemToRemove): CoreModelCollectionInterface
    {
        foreach($this->items as $index => $item) {
            $item = $this->getAt($index);
            if(get_class($item) == get_class($itemToRemove)
                && $item->getId() == $itemToRemove->getId()
            ) {
                unset($this->items[$index]);
                $this->items = array_values($this->items);
                $this->chunkRows = count($this->items);
                $this->totalRows--;

                // In case the item is removed inside a loop,
                // we need to adjust the loop position so that it matches the reorganized items
                if($this->position == $index) {
                    $this->position--;
                }
                $this->isAmended = true;
                break;
            }
        }
        return $this;
    }

    /**
     * Reverse the order of all the items in the collection
     *
     * WARNING: Amending a collection this way will make it unreliable for reloading
     *
     * @return self
     * @throws CoreModelException
     * @see isAmended()
     */
    public function reverse(): CoreModelCollectionInterface
    {
        $this->load();
        $this->items = array_reverse($this->items);
        $this->isAmended = true;
        return $this;
    }

    /**
     * Slice the db result set after it has loaded
     *
     * WARNING: Amending a collection this way will make it unreliable for reloading
     *
     * @param int $offset
     * @param int|null $length
     * @return self
     * @throws CoreModelException
     * @see isAmended()
     */
    public function slice(int $offset, ?int $length = null): CoreModelCollectionInterface
    {
        $this->load();
        $this->items = array_slice($this->items, $offset, $length);
        $this->chunkRows = count($this->items);
        $this->isAmended = true;
        return $this;
    }

    /**
     * Sort all the items by a custom function after loading
     *
     * WARNING: Amending a collection this way will make it unreliable for reloading
     *
     * @param Closure $sortfunction
     * @return self
     * @throws CoreModelException
     * @see isAmended()
     */
    public function sortLoadedItems(Closure $sortfunction): CoreModelCollectionInterface
    {
        $this->getItemsArray();
        usort($this->items, $sortfunction);
        return $this;
    }

    /**
     * Returns true if the collection was amended since it was loaded
     *
     * If the collection has been amended, then all filters etc may be invalid
     * and the collection should not be relied on for reloading
     *
     * @return bool
     * @see load()
     */
    public function isAmended(): bool
    {
        return $this->isAmended;
    }

    /**
     * Save a value to all items in the collection;
     *
     * @param string $property The property to udate
     * @param mixed $value The new value which will be applied to all items in the collection
     * @return self
     * @throws CoreModelException
     */
    public function setData(string $property, $value): CoreModelCollectionInterface
    {
        list('property' => $property, 'value' => $value)
            = $this->app->before($this, __FUNCTION__, [
            'property' => $property, 'value' => $value
        ]);
        /** @var CoreModel|null $firstItem */
        if($firstItem = $this->getFirstItem()) {
            $mapping = $firstItem->getDataMapping();
            /** @var array|false $field if the table and field exists in the mapping, it is as a key=>value pair */
            $field = $mapping[$property] ?? false;
            if($field && array_keys($field)[0] == $this->dbTable) {
                // The vales can be updated in bulk
                $field = reset($field);
                $fieldConfig = $firstItem->getFieldConfig($this->dbTable, $field);
                $dbValue = $firstItem->toDbValue($fieldConfig, $value);
                if(!$this->isAmended()) {
                    $this->saveToMainTable((object)["`{$this->dbTable}`.`{$field}`" => $dbValue]);
                }
                else {
                    $this->saveToMainTableOnItemIds((object)["`{$this->dbTable}`.`{$field}`" => $dbValue]);
                }
            }
            elseif ($field) {
                // We cannot save in bulk, so need to itereate over each item and set the value on each
                foreach($this->getItemsArray() as $object) {
                    $object->setData($property, $value);
                }
            }
        }
        return $this->app->after($this, __FUNCTION__, $this);
    }

    /**
     * Save a set of values to all items in the collection
     *
     * @param stdClass $values The properties to update for all items in the collection, and their new values
     * @return self
     * @throws CoreModelException
     */
    public function setMultipleData(stdClass $values): CoreModelCollectionInterface
    {
        list('values' => $values)
            = $this->app->before($this, __FUNCTION__, [
            'values' => $values
        ]);
        /** @var CoreModel|null $firstItem */
        if($firstItem = $this->getFirstItem()) {
            $mapping = $firstItem->getDataMapping();

            $dbValues = new stdClass();
            $notInMainTable = new stdClass();

            foreach ($values as $property => $value) {
                /** @var array|false $field if the table and field exists in the mapping, it is as a key=>value pair */
                $field = $mapping[$property] ?? false;

                if($field && array_keys($field)[0] == $this->dbTable) {
                    // The vales can be updated in bulk
                    $field = reset($field);
                    $fieldConfig = $firstItem->getFieldConfig($this->dbTable, $field);
                    $dbValue = $firstItem->toDbValue($fieldConfig, $value);
                    $dbValues->{"`{$this->dbTable}`.`{$field}`"} = $dbValue;

                }
                else {
                    $notInMainTable->$property = $value;
                }
            }
            if(!$this->isAmended()) {
                $this->saveToMainTable($dbValues);
            }
            else {
                $this->saveToMainTableOnItemIds($dbValues);
            }
            foreach ($notInMainTable as $property => $value) {
                foreach($this->getItemsArray() as $object) {
                    $object->setData($property, $value);
                }
            }
        }
        return $this->app->after($this, __FUNCTION__, $this);
    }

    /**
     * Mass delete from the database
     *
     * Warning: This 'Delete' method will force delete the entries from the database,
     * without additional sanity checks etc.
     *
     * To delete each object the safe way, use deleteEach() instead
     *
     * @return self
     * @throws CoreModelException
     * @see deleteEach()
     */
    public function massDelete(): CoreModelCollectionInterface
    {
        $this->app->before($this, __FUNCTION__, []);
        $tp = CoreModel::$tablePrefix;
        $itemIds = $this->getItemIds();
        if ($itemIds) {
            $bindValues = [];
            $delete = "DELETE `{$this->dbTable}`.*\n";
            $from = "FROM\n`{$tp}{$this->dbTable}` AS `{$this->dbTable}`\n" . implode("\n", $this->getJoins()) . "\n";
            $where = $this->getFilters() ? "WHERE\n" . $this->prepareFiltersQuery('and', $this->getFilters(), $bindValues) . "\n" : '';
            $sql = $delete . $from . $where;
            $statement = $this->mysqli->prepare($sql);
            if (!$statement) {
                throw new CoreModelException($this->mysqli->error);
            }

            if($bindValues) {
                $types = str_repeat('s', count($bindValues));
                $statement->bind_param($types, ...$bindValues);
            }
            CoreModel::log($sql, 'debug', $bindValues);
            if (!$statement->execute()) {
                throw new CoreModelException($statement->error);
            }
            $statement->close();
            CoreModel::$queryCount++;
        }
        return $this->app->after($this, __FUNCTION__, $this);
    }

    /**
     * Delete each object in a collection
     *
     * @return self
     * @throws CoreModelException
     */
    public function deleteEach(): CoreModelCollectionInterface
    {
        $this->app->before($this, __FUNCTION__, []);
        if( !$this->hasLoaded()) {
            $this->load();
        }
        foreach ($this->getItemsArray() as $item) {
            $item->delete();
        }
        $this->unload();
        return $this->app->after($this, __FUNCTION__, $this);
    }

    /**
     * Get the name of the model class used by the collection
     *
     * @return class-string<CoreModel>
     */
    public function getModel(): string
    {
        return $this->model;
    }

    /**
     * Get the current or last used DB query
     *
     * @return string|null
     */
    public function getQuery(): ?string
    {
        return $this->query;
    }

    /**
     * Find a linked collection by reference
     *
     * @param string $reference
     * @return CoreModelCollection|null
     */
    public function getFollower(string $reference):?CoreModelCollection
    {
        if(isset($this->followers[$reference])) {
            return $this->followers[$reference]->collection;
        }
        return null;
    }

    /**
     * Include another collection based on its one-2-many relation
     *
     * @param string $reference
     * @param CoreModelCollection|null $collection
     * @param string|null $model
     * @param string|null $foreignKey
     * @param array $filters
     * @param callable|null $callback
     * @return CoreModelCollection|null
     * @throws CoreModelException
     * @throws SubcollectionFilterException
     * @see self::$followers
     */
    public function linkCollection(
        string    $reference,
        ?CoreModelCollectionInterface $collection = null,
        ?string   $model = null,
        ?string   $foreignKey = null,
        array     $filters = [],
        ?callable $callback = null
    ): ?CoreModelCollectionInterface
    {
        /** @var class-string<CoreModel> $thisModel */
        $thisModel = $this->getModel();
        $one2Many = $thisModel::hasMany($reference, $model, $foreignKey, $filters, $callback);
        if($one2Many && empty($this->followers[$reference])) {
            if($collection) {
                if(
                    $collection->getFilters()
                    || $collection->getInnerJoins()
                    || $collection->getStart()
                    || $collection->getLimit() !== null
                ) {
                    throw new SubcollectionFilterException('The collection passed to CoreModelCollection::linkCollection() cannot have filters, inner-joins or result set limitations.');
                }
            }
            /** @var class-string<CoreModel>|null $linkedModel */
            $linkedModel = $one2Many['model'] ?? null;
            if (is_a($linkedModel, CoreModelInterface::class, true)) {

                /** @var CoreModelCollection $linkedCollection */
                $linkedCollection = $collection && $collection->getModel() === $linkedModel
                    ? $collection :
                    $this->app->getModelCollection($linkedModel);

                $this->followers[$reference] = (object)[
                    'collection' => $linkedCollection,
                    'groupedItems' => null
                ];

                $linkedCollection->willFollow($this, $reference);

                if($this->hasLoaded()) {
                    $this->loadIncludedCollection($reference);
                }
            }
        }
        return $this->followers[$reference]->collection ?? null;
    }

    /**
     * @param CoreModelCollectionInterface $collection
     * @param string $reference
     * @param string|null $model
     * @param string|null $foreignKey
     * @param array $filters
     * @param callable|null $callback
     * @return self
     * @throws CoreModelException
     * @throws Exception
     */
    public function willFollow(
        CoreModelCollectionInterface $collection,
        string  $reference,
        ?string $model = null,
        ?string $foreignKey = null,
        array   $filters = [],
        ?callable $callback = null
    ): CoreModelCollectionInterface
    {
        if($this->following) {
            if( $collection !== $this->following->collection || $reference !== $this->following->reference) {
                throw new CoreModelException('This collection is already following another collection');
            }
        }
        else {
            $one2Many = $collection->getModel()::hasMany($reference, $model, $foreignKey, $filters, $callback);
            if(!$one2Many) {
                throw new CoreModelException('Invalid one2many configuration');
            }
                $this->setFilters($one2Many['filters'] ?? []);
                $this->lockFilters();
                $this->addSortOrder($this->getModel()::getDbPrimaryKeyField());
                if(isset($one2Many['callback']) && is_callable($one2Many['callback'])){
                    $one2Many['callback']($this);
                }
            $this->following = (object)[
                'reference' => $reference,
                'collection' => $collection,
                'configuration' => $one2Many,
            ];
        }
        return $this;
    }


    /**
     * Looks up the collection for which this is the subcollection source
     *
     * @return CoreModelCollection|null
     */
    public function getCollectionThisIsSubCollectionSourceFor(): ?CoreModelCollection
    {
        if ($this->following) {
            $follows = $this->following->collection;
            $reference = $this->following->reference;
            if($follows->getFollower($reference) === $this) {
                return $follows;
            }
        }
        return null;
    }

    /**
     * Check if this collection is linked to (following) another collection
     *
     * @return object{reference: string, configuration: array, collection: CoreModelCollection}|null
     */
    public function follows(): ?object
    {
        return $this->following;
    }

    /**
     * Reset filters and prepare the collection for reloading
     *
     *  The collection items unload when a filter changes.
     *
     * @return self
     */
    public function reset(): CoreModelCollectionInterface
    {
        $this->resetFilters();
        $this->unload();
        return $this;
    }
}