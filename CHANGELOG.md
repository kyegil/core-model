# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/).

## [Unreleased]

## [4.0.2] - 2025-03-17

- Saving DateTimeImmutable in correct timezone

## [4.0.1] - 2025-01-29

- Corrected signature of MixedCollection::setFilters()

## [4.0.0] - 2024-12-19

- Updated the Interfaces
- Added typehints
- General re-organising of methods
- Improved included collections
- Updated documentation

## [3.2.6] - 2024-11-16

- Added query count for debugging

## [3.2.5] - 2024-09-24

### Added

- Added option to sort null values as max value

### Changed

- Removed cached Eav Value object when deleted

## [3.2.4] - 2024-09-05

### Changed

- Prevented creating a CoreModel object with null as id

## [3.2.3] - 2024-08-10

### Added

- Added hasData method
- Added coverData method

## [3.2.2] - 2024-07-02

### Changed

- Renamed foreach method to forEach

## [3.2.1] - 2024-06-16

### Added

- Adding safety filters to loadFromArray method

## [3.2.0] - 2024-04-28

### Added

- `allowMultiple` property config option
- `foreach` method on collections
- extra (currently unused) argument `$collection` on CoreModelCollection::linkCollection(), which in the future may allow for linking existing collections
- added logger
- added CoreModelException

## [3.1.4] - 2023-11-22

### Changed

- Updating cached EAV values after saving

## [3.1.3] - 2023-11-09

### Changed

- The Default value of the `rawDataContainer` field configuration has been changed
  to match the default namespace used in CoreModelCollection::addField()
- Allowed creating empty models

## [3.1.2] - 2023-11-05

### Changed

- Now Returning empty collections if foreign key is null

## [3.1.1] - 2023-09-05

### Changed

- Fixed problem with missing indexes on foreign collections

## [3.1.0] - 2023-07-22

### Added

- Added methods for preloading of one-to-many-relations

## [3.0.7] - 2023-06-09

### Changed

- Fixed problem with ordering on custom expressions

## [3.0.6] - 2023-04-13

### Changed

- Unloading amended collections

## [3.0.5] - 2023-03-07

### Changed

- Fixed issue with counts not being reset when unloading items

## [3.0.4] - 2022-11-17

### Added

- !! CRITICAL SAFEGUARD !! Requires filters to be in place before updating tables

### Changed

- !! CRITICAL FIX !! of issue which was resetting (and thereby possibly removing) filters after save operations.
  - For further security all filters should be locked before performing any save operations.
  - @see CoreModelCollection::lockFilters()

## [3.0.3] - 2022-11-16

### Added

- Added this changelog

### Changed

- Fixed issue with iterating over raw stdClass items.
